#Makefile Linux
EXEC = NoMineSky
COMPILO = g++
OPTION = -Wall -Wno-switch -O3

#Path
PATH_OBJ = Build
PATH_ENGINE = Engine
PATH_GAME = Game

#Librairies
LIB_DEP = -lSDL2 -lSDL2_image -lGLEW -lGL
LIB_GAME = -lnoise -lpthread
#Liste fichiers .cpp et .O
SRC = $(shell find ./ -type f -name '*.cpp')
OBJ = $(SRC:./%.cpp=$(PATH_OBJ)/%.o)

# Liste des dossiers
DIR_ENGINE = Behavior Camera Config Controller Graphic Input System Mesh Light Time Utility
DIR_GAME = Block Map

#Actions
all : $(EXEC)
	./$(EXEC)

valgrind : $(EXEC)
	valgrind ./$(EXEC)

debug : $(EXEC)
	gdb ./$(EXEC)

$(EXEC) : $(OBJ)
	@echo "\033[31m[Link] $(EXEC)\033[00m"
	@$(COMPILO) $(OPTION) -std=c++11 -o $@ $^ $(LIB_DEP) $(LIB_GAME)

$(PATH_OBJ)/%.o: ./%.cpp
	@echo "\033[32m[Build] $@\033[00m"
	@$(COMPILO) $(OPTION) -std=c++11 -o $@ -c $< -I$(PATH_ENGINE) -I$(PATH_GAME)

init :
	mkdir -p $(PATH_OBJ) $(PATH_OBJ)/$(PATH_ENGINE) $(DIR_ENGINE:%=$(PATH_OBJ)/$(PATH_ENGINE)/%) $(PATH_OBJ)/$(PATH_GAME) $(DIR_GAME:%=$(PATH_OBJ)/$(PATH_GAME)/%)

clean :
	@echo "Suppression des .o et fichiers temporaires"
	@find . -name '*.o' -exec rm  {} \;
	@find . -name '*~' -exec rm  {} \;

install :
	sudo apt-get install g++ libsdl2-dev libsdl2-image-dev libglew-dev libglm-dev libnoise-dev

commit :
	git commit -a
	git push

add :
	@git add . --verbose

doc :
	doxygen
