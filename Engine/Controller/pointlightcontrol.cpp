#include "pointlightcontrol.h"

namespace engine
{




PointLightControl::PointLightControl(GameObject* go, PointLight* l)
:Behavior(go), light(l)
{
	currentSetting = Settings::AMBIENT;

	printSetting();
}


void PointLightControl::update()
{
	if(Keyboard::isKeyPressed(Keyboard::LEFT))
		nextSetting(true);
	if(Keyboard::isKeyPressed(Keyboard::RIGHT))
		nextSetting(false);

	if(Keyboard::isKeyPressed(Keyboard::UP))
		incrementSetting(true);
	if(Keyboard::isKeyPressed(Keyboard::DOWN))
		incrementSetting(false);
}



string PointLightControl::getSettingString() const
{
	switch(currentSetting)
	{
		case AMBIENT :     return "AMBIANT";
		case ATTENUATION : return "ATTENUATION";

		default : return "ERROR";
	}
}



float PointLightControl::getValue() const
{
	switch(currentSetting)
	{
		case AMBIENT :     return light->getAmbient();
		case ATTENUATION : return light->getAttenuation();

		default : return 0;
	}
}


void PointLightControl::setValue(float val) const
{
	switch(currentSetting)
	{
		case AMBIENT :     light->setAmbient(val);     break;
		case ATTENUATION : light->setAttenuation(val); break;
	}
}




void PointLightControl::nextSetting(bool next)
{
	int index = (int)currentSetting;
	
	index += (next ? 1 : -1);
	
	if(index < 0)
		index = COUNT-1;
	else if(index >= COUNT)
		index = 0;

	currentSetting = (Settings)index;
	printSetting();
}



void PointLightControl::incrementSetting(bool positive)
{
	float offset = getValue() * 1/10 * (positive ? 1 : -1);
	setValue(getValue() + offset);
	printSetting();
}



void PointLightControl::printSetting() const
{
	cout << getSettingString() << " = " << getValue() << endl;
}


}