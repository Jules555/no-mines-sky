#include "firstpersoncamera.h"

namespace engine
{


FirstPersonCamera::FirstPersonCamera(GameObject* go, Camera* cam)
:Behavior(go),camera(cam)
{

}

FirstPersonCamera *FirstPersonCamera::clone() const
{
    return new FirstPersonCamera(*this);
}


void FirstPersonCamera::update()
{
    // Calcul des vecteurs si on ne l'a jamais fait
    if(!vectorsCalculated)
    {
        computeVectors();
        vectorsCalculated = true;
    }

    // Mouvement de la souris
    if(Mouse::delta.x != 0 || Mouse::delta.y != 0)
        updateLook();

    // Calcul du mouvement
    vec3 movement;
    float factor = Time::getDeltaTime() * (Keyboard::isKeyPressed(Keyboard::SHIFT) ? speedFast : speed);

    if(Keyboard::isKeyPressed(Keyboard::Z))     movement += forward * factor;
    if(Keyboard::isKeyPressed(Keyboard::S))     movement -= forward * factor;
    if(Keyboard::isKeyPressed(Keyboard::Q))     movement += left * factor;
    if(Keyboard::isKeyPressed(Keyboard::D ))    movement -= left * factor;
    if(Keyboard::isKeyPressed(Keyboard::SPACE)) movement += Global::UP * factor;
    if(Keyboard::isKeyPressed(Keyboard::ALT))   movement -= Global::UP * factor;

    // On se met à jour en fonction du mouvement
    gameObject->transform.translate(movement);
    target = gameObject->transform.getPosition() + forward;
    camera->setLook(gameObject->transform.getPosition(),target,Global::UP);
}


void FirstPersonCamera::computeVectors()
{
    // Coordonnées sphériques -> cartésiennes

    // Calcul angle forward
    double r_temp = cos(Angle::toRadians(phi));
    forward.x = r_temp*sin(Angle::toRadians(theta));
    forward.z = r_temp*cos(Angle::toRadians(theta));
    forward.y = -sin(Angle::toRadians(phi));

    //qDebug() << theta << " " << forward;

    // Calcul angle left
    left = glm::normalize(glm::cross(Global::UP,forward));

    // Calcul target
    target = gameObject->transform.getPosition() + forward;
}




void FirstPersonCamera::updateLook()
{
    vec2 dp = Mouse::delta;

    // Mise à jour des angles du regard
    theta -= dp.x;
    phi += dp.y;

    if (phi > 85)
        phi = 85;
    else if (phi < -85)
        phi = -85;

    // Mise à jour des angles
    computeVectors();

    // On met à jour le gameobject
    vec3 rot = gameObject->transform.getRotation();
    rot.y = theta;
    gameObject->transform.setRotation(rot);
}




// void FirstPersonCamera::save(QJsonObject& json) const
// {
//     json["type"] = "FirstPersonCamera";
//     json["speed"] = speed;
//     json["speedFast"] = speedFast;
//     json["theta"] = theta;
//     json["phi"] = phi;
// }


}