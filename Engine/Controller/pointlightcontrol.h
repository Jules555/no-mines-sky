#pragma once

#include "Behavior/behavior.h"
#include "Light/pointlight.h"
#include "Input/keyboard.h"


namespace engine
{
	class PointLightControl : public Behavior
	{
		protected :

			enum Settings { AMBIENT, ATTENUATION, COUNT };

			PointLight* light;
			Settings currentSetting;

			virtual void update();

			string getSettingString() const;

			float getValue() const;
			void setValue(float) const;

			void nextSetting(bool next);
			void incrementSetting(bool positive);

			void printSetting() const;

		public :

			PointLightControl(GameObject* go, PointLight* l);

	};
}