#pragma once

#include "Utility/glm.h"
#include "Utility/angle.h"
#include "Camera/camera.h"
#include "Input/input.h"
#include "Behavior/behavior.h"
#include "Config/config.h"

namespace engine
{
    class FirstPersonCamera : public Behavior
    {

    protected :

        Camera* camera;

        float speed = 1.0f;
        float speedFast = 5.0f;

        bool vectorsCalculated = false;

        float theta = 0.0f;
        float phi = 0.0f;

        vec3 target;
        vec3 forward;
        vec3 left;

    public:

        FirstPersonCamera(GameObject* go, Camera* cam);

        virtual FirstPersonCamera* clone() const;

        virtual void update();
        void computeVectors();
        void updateLook();

        void setTheta(float t) {theta = t;}

        //virtual void save(QJsonObject &) const;
    };
}