#include "gamewindow.h"

namespace engine
{




GameWindow::GameWindow()
{
	// Attributes
	running = false;

	// Initialisation SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(-1);
    }

	// Initialisation contexte OpenGL
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GL_SetSwapInterval(1); // V-Sync

	// Récupération de la résolution de l'écran
	SDL_DisplayMode currentScreen;
	SDL_GetCurrentDisplayMode(0, &currentScreen);
	Config::screenWidth = currentScreen.w;
	Config::screenHeight = currentScreen.h;

	// Fullscreen ou non ?
	Config::windowWidth = Config::screenWidth;
	Config::windowHeight = Config::screenHeight;

	// Création fenêtre
	window = SDL_CreateWindow(
		"Game Engine",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		Config::windowWidth,
		Config::windowHeight,
		// SDL_WINDOW_SHOWN| SDL_WINDOW_OPENGL
		// SDL_WINDOW_BORDERLESS | SDL_WINDOW_OPENGL
		SDL_WINDOW_FULLSCREEN | SDL_WINDOW_OPENGL
	);

	// Création contexte OpenGL
	glContext = SDL_GL_CreateContext(window);
	if(glContext == 0)
	{
	    cout << SDL_GetError() << endl;
	    SDL_DestroyWindow(window);
	    SDL_Quit();
	    exit(-1);
	}

	// Initialisation GLEW
    glewExperimental = GL_TRUE;
	GLenum initGlew(glewInit());
	if(initGlew != GLEW_OK)
	{
	    cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initGlew) << endl;
	    SDL_GL_DeleteContext(glContext);
	    SDL_DestroyWindow(window);
	    SDL_Quit();
	    exit(-1);
	}

	// Initialisation
	initialize();
}


GameWindow::~GameWindow()
{
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
    SDL_Quit();
}


void GameWindow::initialize()
{
	// Init OpenGL
	glClearColor(Config::clearColor.x,Config::clearColor.y,Config::clearColor.z,1.0f);

	glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable (GL_BLEND);

    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glShadeModel(GL_FLAT);

	// Définition camera
	camera.setPerspective(60, (float)Config::screenWidth/Config::screenHeight, 0.001, 10000);
    scene.setCamera(&camera);

    // Init graphics
    MaterialManager::initialize();

	// Init inputs
	Keyboard::initialize();
	Mouse::initialize();

	// Init utilities
	Random::initialize();
}


// Boucle de jeu
void GameWindow::run()
{
	running = true;
	scene.start();

	while(running)
    {
    	// Mise à jour temps écoulé
    	Time::update();

        // Framerate
        // static float fps_timer  = 0.0f;
        // static uint fps_counter = 0;
        // fps_timer += Time::getRealDeltaTime();
        // fps_counter++;
        // if(fps_timer > 1.0f)
        // {
        // 	cout << int(fps_counter/fps_timer) << " fps" << endl;
        // 	fps_timer = 0.0f;
        // 	fps_counter = 0;
        // }

        // Check events
    	events();

        // Systems
		update();
		render();
    }
}


void GameWindow::events()
{
	Mouse::newFrame();

	// Boucle d'événement
    while(SDL_PollEvent(&event))
    {
	    switch(event.type)
	    {
	    	case SDL_WINDOWEVENT :
	    		if(event.window.event == SDL_WINDOWEVENT_CLOSE)
	    			running = false;
	    		break;

	    	case SDL_KEYDOWN :
                if(event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
	    			running = false;
	    		else
	    			Keyboard::keyPressed(event.key.keysym.sym);
	    		break;

            case SDL_KEYUP :
                Keyboard::keyReleased(event.key.keysym.sym);
                break;

	    	case SDL_MOUSEMOTION :
	    		Mouse::mouseMoved(event.motion);
	    		break;
	    }
	}
}


void GameWindow::update()
{
	scene.update();
}


void GameWindow::render()
{
	// Clean affichage
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Récupération des shaders actifs, on calcule un rendu pour chaque
    vector<Shader*> activeShaders = ShaderManager::getActiveShaders();;
    for(unsigned int i=0; i<activeShaders.size(); ++i)
    {
    	// On active le shader
    	activeShaders[i]->use();

	    // On passe la matrice de projection au shader
	    activeShaders[i]->setUniform("projection",camera.getPerspective());

	    // On met à jour les lumières
	    LightManager::updateLights(activeShaders[i]);

	    // Rendu de la scène
		scene.render();
	}

	glDepthFunc(GL_LEQUAL);
	scene.renderSkyBox();
	glDepthFunc(GL_LESS);

	// Swap Buffer
	SDL_GL_SwapWindow(window);
}


}