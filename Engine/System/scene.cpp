#include "scene.h"
#include "component.h"

namespace engine
{




Scene* Scene::current = NULL;


Scene::Scene()
{
}


Scene::~Scene()
{
    for(unsigned int i=0; i<gameObjects.size(); ++i)
        delete gameObjects[i];
    gameObjects.clear();
}



void Scene::start()
{
    Scene::current = this;

    skybox.start();
    cubemap = ShaderManager::getShader(ShaderManager::CUBEMAP);

    // On start tous les components
    for(unsigned int i=0; i<gameObjects.size(); ++i)
        gameObjects[i]->start();
}


void Scene::update()
{
    Scene::current = this;

    for(unsigned int i=0; i<toUpdate.size(); ++i)
        toUpdate[i]->update();
}


void Scene::render()
{
    Scene::current = this;

    // Rendu components
    for(unsigned int i=0; i<toRender.size(); ++i)
        toRender[i]->render();
}


void Scene::renderSkyBox()
{
    if(!skybox.isLoaded())
        return;

    cubemap->use();
    cubemap->setUniform("projection",camera->getPerspective());
    cubemap->setUniform("modelview",mat4(mat3(camera->getLook())));

    skybox.render();
}


void Scene::addGameObject(GameObject *go)
{
    gameObjects.push_back(go);
    go->setScene(this);
}


void Scene::setCamera(Camera *cam)
{
    if(camera == NULL)
        camera = cam;
    else
        cerr << "Il ne peut y avoir qu'une seule caméra dans une scène !" << endl;
}


void Scene::setSkyBox(string filename)
{
    skybox.load(filename);
}



// bool Scene::save(QString filename) const
// {
//     qDebug() << "Save into " << filename;

//     QJsonObject json;
//     QJsonArray gos;
//     for(int i=0; i<gameObjects.size(); ++i)
//     {
//         QJsonObject go;
//         gameObjects[i]->save(go);
//         gos.append(go);
//     }
//     json["gameObjects"] = gos;

//     QFile file(filename);

//     if (!file.open(QIODevice::WriteOnly))
//     {
//         qWarning("Couldn't open save file.");
//         return false;
//     }

//     QJsonDocument jsonFile(json);
//     file.write(jsonFile.toJson());

//     return true;
// }


// bool Scene::load(QString filename)
// {
//     QFile loadFile(filename);

//     if (!loadFile.open(QIODevice::ReadOnly))
//     {
//         qWarning("Couldn't open save file.");
//         return false;
//     }

//     QJsonDocument jsonFile(QJsonDocument::fromBinaryData(loadFile.readAll()));
//     QJsonObject json = jsonFile.object();

//     return true;
// }


}