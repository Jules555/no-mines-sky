#include "transform.h"

namespace engine
{



Transform::Transform()
{
    position = {0.0f,0.0f,0.0f};
    rotation = {0.0f,0.0f,0.0f};
    scaling  = {1.0f,1.0f,1.0f};
}


void Transform::setPosition(const vec3& p)
{
    position = p;
    computeMatrix();
}

void Transform::setRotation(const vec3& r)
{
    rotation = r;
    computeMatrix();
}

void Transform::setScaling(const vec3& s)
{
    scaling = s;
    computeMatrix();
}

void Transform::translate(const vec3& t)
{
    position += t;
    computeMatrix();
}

void Transform::rotate(const vec3& r)
{
    rotation += r;
    computeMatrix();
}

void Transform::scale(const vec3& s)
{
    scaling += s;
    computeMatrix();
}


void Transform::computeMatrix()
{
    matrix = mat4();
    matrix = glm::translate(matrix,position);
    matrix = glm::scale(matrix,scaling);
    matrix = glm::rotate(matrix,Angle::toRadians(rotation.y),vec3(0,1,0));
    matrix = glm::rotate(matrix,Angle::toRadians(rotation.x),vec3(1,0,0));
    matrix = glm::rotate(matrix,Angle::toRadians(rotation.z),vec3(0,0,1));
}



// void Transform::save(QJsonObject& json) const
// {
//     QJsonObject obj;

//     // Position
//     obj["x"] = position.x();
//     obj["y"] = position.y();
//     obj["z"] = position.z();
//     json["position"] = obj;

//     // Rotation
//     obj["x"] = rotation.x();
//     obj["y"] = rotation.y();
//     obj["z"] = rotation.z();
//     json["rotation"] = obj;

//     // Scaling
//     obj["x"] = scaling.x();
//     obj["y"] = scaling.y();
//     obj["z"] = scaling.z();
//     json["scaling"] = obj;
// }


}