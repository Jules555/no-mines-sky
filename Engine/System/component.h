#pragma once

#include <iostream>

using namespace std;

#include "gameobject.h"


namespace engine
{
    class Component
    {

    protected :

        GameObject* gameObject;

    public:

        Component(GameObject* g); // Le component est crée et ajouté au GameObject
        virtual ~Component();

        virtual Component* clone() const {return new Component(*this);};

        virtual void subscribe() {}

        virtual void start() {}
        virtual void update() {}
        virtual void render() const {}

        inline const GameObject* getGameObject() const {return gameObject;}
        inline void setGameObject(GameObject* go) {gameObject = go;}

        //virtual void save(QJsonObject&) const {}

    };
}
