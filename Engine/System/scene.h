#pragma once

#include <iostream>
#include <vector>

#include "Camera/camera.h"
#include "Graphic/skybox.h"

using namespace std;

namespace engine
{
    class GameObject;
    class Component;

    class Scene
    {

    private :

        vector<GameObject*> gameObjects;

        vector<Component*> toUpdate;
        vector<Component*> toRender;

        SkyBox skybox;
        Shader* cubemap;

    public:

        Scene();
        ~Scene();

        static Scene* current;
        Camera* camera = NULL;

        void renderSkyBox();

        void start();
        void update();
        void render();

        void setCamera(Camera* cam);
        void setSkyBox(string filename);

        void addGameObject(GameObject* go);

        inline void subscribeUpdate(Component* co) {toUpdate.push_back(co);}
        inline void subscribeRender(Component* co) {toRender.push_back(co);}

        // bool save(QString filename) const;
        // bool load(QString filename);

    };
}