#include "component.h"
#include "gameobject.h"

namespace engine
{

Component::Component(GameObject* g)
{
    g->addComponent(this);
    subscribe();
}


Component::~Component()
{
	
}

}