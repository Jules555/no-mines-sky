#pragma once

#include "Utility/glm.h"
#include "Utility/angle.h"

using namespace glm;

namespace engine
{
    class Transform
    {

    private :

        vec3 position;
        vec3 rotation;
        vec3 scaling;

        mat4 matrix;

        void computeMatrix();

    public:

        Transform();

        inline const vec3& getPosition() const  {return position;}
        inline const vec3& getRotation() const {return rotation;}
        inline const vec3& getScaling() const {return scaling;}
        void setPosition(const vec3&);
        void setRotation(const vec3&);
        void setScaling(const vec3&);
        void translate(const vec3&);
        void rotate(const vec3&);
        void scale(const vec3&);

        inline const mat4& getMatrix() const {return matrix;}

        //void save(QJsonObject&) const;

    };
}
