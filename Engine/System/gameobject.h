#pragma once

#include <iostream>
#include <vector>
#include <typeinfo>

using namespace std;

#include "transform.h"
#include "scene.h"


namespace engine
{
    class Scene;
    class Component;

    class GameObject
    {

    public:

        Scene* scene = NULL;

        vector<Component*> components;

        void start();

    public:

        Transform transform;

        GameObject(Scene* s);
        ~GameObject();

        GameObject* clone() const;

        void addComponent(Component* co);
        void destroyComponent(Component* co);

        inline Scene* getScene() const {return scene;}
        inline void setScene(Scene* s) {scene = s;}

        inline void setTransform(const Transform &value) {transform = value;}


        // Get component
        template <class T>
        T* getComponent() const
        { 
            // const type_info& type = typeid(T);
            T* to_return;

            for(unsigned int i=0; i<components.size(); ++i)
            {
                to_return = dynamic_cast<T*>(components[i]);
                if(to_return != NULL)
                    return to_return;
            }

            return NULL;
        }


        // void save(QJsonObject&) const;
    };
}