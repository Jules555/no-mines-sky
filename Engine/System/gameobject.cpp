#include "gameobject.h"
#include "component.h"

namespace engine
{




GameObject::GameObject(Scene* s)
:scene(s)
{
    scene->addGameObject(this);
}

GameObject::~GameObject()
{
    for(unsigned int i=0; i<components.size(); ++i)
        delete components[i];
    components.clear();
}


GameObject* GameObject::clone() const
{
    GameObject* out = new GameObject(scene);
    out->transform = transform;

    for(unsigned int i=0; i<components.size(); ++i)
    {
        Component* comp = components[i]->clone();
        out->addComponent(comp);
        comp->subscribe();
    }

    return out;
}


void GameObject::addComponent(Component *co)
{
    components.push_back(co);
    co->setGameObject(this);
}


void GameObject::destroyComponent(Component *co)
{
    for(unsigned int i=0; i<components.size(); ++i)
        if(components[i] == co)
        {
            components[i] = components[components.size()];
            components.pop_back();
            break;
        }
}


void GameObject::start()
{
    for(unsigned int i=0; i<components.size(); ++i)
        components[i]->start();
}


// void GameObject::save(QJsonObject& json) const
// {
//     QJsonObject tr;
//     transform.save(tr);
//     json["transform"] = tr;

//     QJsonArray array;
//     for(int i=0; i<components.size(); ++i)
//     {
//         QJsonObject co;
//         components[i]->save(co);
//         array.append(co);
//     }
//     json["components"] = array;
// }


}