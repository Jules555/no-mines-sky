#pragma once

#include <iostream>
#include <vector>

#include "Utility/glm.h"
#include "Utility/angle.h"
#include "System/transform.h"
#include "Config/global.h"

using namespace std;
using namespace glm;

namespace engine
{

    class Camera
    {

    protected:

        mat4 projection, look;

        float fovy;
        float aspectRatio;
        float nearPlane, farPlane;

        vec3 position, target, normal;


    public:

        Camera();

        void setPerspective(float verticalAngle, float aspectRatio, float nearPlane, float farPlane);
        void setLook(vec3 p, vec3 t, vec3 n);

        inline mat4 getPerspective() const {return projection;}
        inline mat4 getLook() const {return look;}

        inline vec3 getPosition() const {return position;}
    };
}