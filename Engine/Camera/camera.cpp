#include "camera.h"

namespace engine
{

Camera::Camera()
{
    setPerspective(60.0f,16.0f/9.0f,0.01f,1000000.0f);
    setLook(vec3(0.0f,0.0f,0.0f),vec3(0.0f,0.0f,-1.0f),Global::UP);
}


void Camera::setPerspective(float verticalAngle, float ratio, float near, float far)
{
    fovy = Angle::toDegrees(verticalAngle);
    aspectRatio = ratio;
    nearPlane = near;
    farPlane = far;

    projection = glm::perspective(fovy,ratio,nearPlane,farPlane);
}


void Camera::setLook(vec3 e, vec3 t, vec3 n)
{
    position = e;
    target = t;
    normal = n;

    look = glm::lookAt(position,target,normal);
}


}