#include "behavior.h"

namespace engine
{


Behavior::Behavior(GameObject* go)
:Component(go)
{
    subscribe();
}


void Behavior::subscribe()
{
    gameObject->getScene()->subscribeUpdate(this);
}



}