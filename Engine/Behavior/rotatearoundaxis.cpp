#include "rotatearoundaxis.h"

namespace engine
{


RotateAroundAxis::RotateAroundAxis(GameObject* go)
:Behavior(go)
{
    rotateY = true;
}


RotateAroundAxis* RotateAroundAxis::clone() const
{
    return new RotateAroundAxis(*this);
}


void RotateAroundAxis::update()
{
    float angle = Time::getDeltaTime() * speed;
    gameObject->transform.rotate({rotateX ? angle : 0, rotateY ? angle : 0, rotateZ ? angle : 0});
}



// void RotateAroundAxis::save(QJsonObject& json) const
// {
//     json["type"] = "RotateAroundAxis";
//     json["x"] = rotateX;
//     json["y"] = rotateY;
//     json["z"] = rotateZ;
//     json["speed"] = speed;
// }


}