#pragma once

#include "behavior.h"

namespace engine
{

	class RotateAroundAxis : public Behavior
	{

	public:

	    bool rotateX = false;
	    bool rotateY = false;
	    bool rotateZ = false;

	    float speed = 100.0f;

	    RotateAroundAxis(GameObject* go);

	    virtual RotateAroundAxis* clone() const;

	    virtual void update();

	    //virtual void save(QJsonObject &) const;
	};
}