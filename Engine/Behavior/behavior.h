#pragma once

#include "System/component.h"
#include "System/gameobject.h"

#include "Time/time.h"

namespace engine
{

	class Behavior : public Component
	{

	public:

	    Behavior(GameObject* go);

	    virtual void subscribe();

	};

}