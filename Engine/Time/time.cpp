#include "time.h"

namespace engine
{



Time* Time::instance = NULL;


Time::Time()
{
    if(instance == NULL)
    {
        prevTime = SDL_GetTicks();
        instance = this;
    }
    else
        delete(this);
}


void Time::checkInstance()
{
    if(instance == NULL)
        instance = new Time();
}


void Time::update()
{
    checkInstance();

    // Calcul temps écoulé
    instance->currentTime = SDL_GetTicks();
    int delta = instance->currentTime - instance->prevTime;
    int min_delta = ((float)1000/Config::MAX_FRAMERATE);

    // Bloqué à 60fps
    if(delta < min_delta)
        SDL_Delay(min_delta-delta);

    // Mise à jour temps
    instance->currentTime = SDL_GetTicks();
    delta = instance->currentTime - instance->prevTime;

    instance->realDeltaTime = (float)delta/1000;
    instance->deltaTime = instance->realDeltaTime * instance->timeScale;

    instance->prevTime = instance->currentTime;
}



void Time::setTimeScale(float s)
{
    checkInstance();
    instance->timeScale = s;
}


float Time::getRealDeltaTime()
{
    checkInstance();
    return instance->realDeltaTime;
}


float Time::getDeltaTime()
{
    checkInstance();
    return instance->deltaTime;
}


}