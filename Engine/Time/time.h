#pragma once

#include <iostream>
#include <SDL2/SDL.h>

using namespace std;

#include "Config/config.h"

namespace engine
{
    class Time
    {

    private:

        // Singleton
        static Time* instance;
        Time();

        // Gestion du temp
        int prevTime = 0;
        int currentTime = 0;

        float timeScale = 1.0f;

        float realDeltaTime = 0.0f;
        float deltaTime = 0.0f;


    public:

        static void checkInstance();

        static void update();

        static void setTimeScale(float);

        static float getDeltaTime();
        static float getRealDeltaTime();

    };
}