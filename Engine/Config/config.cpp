#include "config.h"

namespace engine
{


// PATHS
const string Config::PATH_SHADERS = "Resources/Shaders";
const string Config::PATH_SKYBOX  = "Resources/Skybox";

// CONSTANTES
const int Config::MAX_FRAMERATE = 60;

// WINDOW
int Config::windowWidth = 1200;
int Config::windowHeight = 675;
int Config::screenWidth = 0;
int Config::screenHeight = 0;

// GRAPHIC
float Config::gamma = 1.0;
vec3 Config::clearColor = vec3(0.0f,0.0f,0.0f);


}