#pragma once

#include <glm/glm.hpp>

using namespace glm;

namespace engine
{
	class Global
	{

	private:

	    Global();

	public :

	    static const vec3 UP;

	};
}