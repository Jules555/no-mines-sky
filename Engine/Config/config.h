#pragma once

#include <string>

#include "Utility/glm.h"

using namespace std;
using namespace glm;

namespace engine
{
	class Config
	{

	private:

	    Config();


	public:

		// PATHS
	    static const string PATH_SHADERS;
	    static const string PATH_SKYBOX;

		// CONSTANTES
	    static const int MAX_FRAMERATE;

	    // WINDOW
	    static int screenWidth, screenHeight;
	    static int windowWidth, windowHeight;

	    // GRAPHIC
	    static float gamma;
	    static vec3 clearColor;

	};
}
