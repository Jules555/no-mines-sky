#pragma once

#include <iostream>
#include <GL/glew.h>
#include <SDL2/SDL.h>

using namespace std;

#include "System/scene.h"
#include "Time/time.h"
#include "Input/input.h"
#include "Utility/random.h"
#include "Utility/glm.h"
#include "Graphic/graphic.h"
#include "Mesh/meshes.h"
#include "Light/lights.h"
#include "Light/lightmanager.h"
#include "Behavior/behaviors.h"
#include "Controller/controllers.h"

namespace engine
{
	class GameWindow
	{
		private :

			SDL_Window* window;
			SDL_Event event;
			SDL_GLContext glContext;

			bool running;

			Scene scene;
			Camera camera;


		public :

			GameWindow();
			~GameWindow();

			void initialize();

			// Boucle de jeu
			void run();
			void events();
			void update();
			void render();

			Scene* getScene() {return &scene;}
			Camera* getCamera() {return &camera;}
	};
}