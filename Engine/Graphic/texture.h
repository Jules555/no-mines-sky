#pragma once

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/glew.h>

using namespace std;

namespace engine
{
	class Texture
	{
		private :

			GLuint m_id;
			string m_filename;

		public :

			Texture(string filename, bool generateMipmap = false);
			~Texture();

			bool load(bool generateMipmap);

			inline GLuint getID() const {return m_id;}

	};
}
