#include "cubemap.h"

namespace engine
{


CubeMap::CubeMap(string filename)
:m_filename(filename)
{
	load();
}



bool CubeMap::load()
{
	// Chargement de l'image
    SDL_Surface *image = IMG_Load(m_filename.c_str());
    if(image == 0)
    {
        cerr << "Erreur : " << SDL_GetError() << endl;
        return false;
    }

    // Génération de l'ID
    glGenTextures(1, &m_id);

    // Verrouillage
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

    // Initialisation formats
    SDL_PixelFormat* pixelFormat = image->format;
    GLenum formatInternal(0);
    GLenum format(0);

    // Image sans transparence
	if(image->format->BytesPerPixel == 3)
	{
		formatInternal = GL_RGB;

		// Format
	    if(image->format->Rmask == 0xff)
	    	format = GL_RGB;
	    else
	    	format = GL_BGR;
	}

	// Image avec transparence
	else if(image->format->BytesPerPixel == 4)
	{
		formatInternal = GL_RGBA;

		// Format
	    if(image->format->Rmask == 0xff)
	    	format = GL_RGBA;
	    else
	    	format = GL_BGRA;
	}

	// Dans les autres cas, on arrête le chargement
	else
	{
	    cerr << "Erreur, format de l'image inconnu" << endl;
	    SDL_FreeSurface(image);
	    return false;
	}

	// Copie des pixels des sous-images
	SDL_Surface* subImage = SDL_CreateRGBSurface(0,image->w/4,image->h/3,pixelFormat->BitsPerPixel,pixelFormat->Rmask,pixelFormat->Gmask,pixelFormat->Bmask,pixelFormat->Amask);

	SDL_Rect dstRect;
	dstRect.x = dstRect.y = 0;
	dstRect.w = dstRect.h = subImage->w;

	SDL_Rect srcRect;
	srcRect.w = srcRect.h = subImage->w;

	for(GLuint i=0; i<6; i++)
	{
		switch(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i)
		{
			case GL_TEXTURE_CUBE_MAP_POSITIVE_X :
				srcRect.x = subImage->w * 2;
				srcRect.y = subImage->h * 1;
				break;

			case GL_TEXTURE_CUBE_MAP_NEGATIVE_X :
				srcRect.x = subImage->w * 0;
				srcRect.y = subImage->h * 1;
				break;

			case GL_TEXTURE_CUBE_MAP_POSITIVE_Y :
				srcRect.x = subImage->w * 1;
				srcRect.y = subImage->h * 0;
				break;

			case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y :
				srcRect.x = subImage->w * 1;
				srcRect.y = subImage->h * 2;
				break;

			case GL_TEXTURE_CUBE_MAP_POSITIVE_Z :
				srcRect.x = subImage->w * 1;
				srcRect.y = subImage->h * 1;
				break;

			case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z :
				srcRect.x = subImage->w * 3;
				srcRect.y = subImage->h * 1;
				break;
		}

		if(SDL_BlitSurface(image,&srcRect,subImage,&dstRect) != 0)
			cerr << "Erreur : " << SDL_GetError() << endl;

	    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, formatInternal, subImage->w, subImage->h, 0, format, GL_UNSIGNED_BYTE, subImage->pixels);
	}

	// Paramètres textures
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	// On libère les images 
	SDL_FreeSurface(image); 

	return true;

}


}