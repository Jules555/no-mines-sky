#include "shader.h"

namespace engine
{



// Shader::Shader()
// :m_vertexID(0), m_fragmentID(0), m_programID(0)
// {

// }


Shader::Shader(string vertexSrc, string fragmentSrc)
:m_vertexID(0), m_fragmentID(0), m_programID(0),m_vertexSrc(vertexSrc),m_fragmentSrc(fragmentSrc)
{
    load();
}

Shader::Shader(Shader const &s)
{
    // Copie des fichiers sources
    m_vertexSrc = s.m_vertexSrc;
    m_fragmentSrc = s.m_fragmentSrc;

    // Chargement du nouveau shader
    load();
}


Shader::~Shader()
{
    // Destruction du shader
    glDeleteShader(m_vertexID);
    glDeleteShader(m_fragmentID);
    glDeleteProgram(m_programID);
}


void Shader::use()
{
    glUseProgram(m_programID);
    ShaderManager::setCurrentShader(this);
}


bool Shader::load()
{
    // Destruction d'un éventuel ancien Shader
    if(glIsShader(m_vertexID) == GL_TRUE)
        glDeleteShader(m_vertexID);

    if(glIsShader(m_fragmentID) == GL_TRUE)
        glDeleteShader(m_fragmentID);

    if(glIsProgram(m_programID) == GL_TRUE)
        glDeleteProgram(m_programID);

    // Compilation des shaders
    if(!compile(m_vertexID, GL_VERTEX_SHADER, m_vertexSrc))
        return false;
    if(!compile(m_fragmentID, GL_FRAGMENT_SHADER, m_fragmentSrc))
        return false;

    // Création du programme
    m_programID = glCreateProgram();
    glAttachShader(m_programID, m_vertexID);
    glAttachShader(m_programID, m_fragmentID);

    // Linkage du programme
    glLinkProgram(m_programID);

    // Vérification du linkage
    GLint error;
    glGetProgramiv(m_programID, GL_LINK_STATUS, &error);

    // Erreur lors du linkage du program
    if(error != GL_TRUE)
    {
        // Récupération de la taille de l'erreur
        GLint errorLength;
        glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &errorLength);

        // Récupération de l'erreur
        char* error = new char[errorLength + 1];
        glGetProgramInfoLog(m_programID, errorLength, &errorLength, error);
        error[errorLength] = '\0';

        // Affichage de l'erreur
        cout << "PROGRAM LINK ERROR : " << endl << error << endl;
        delete[] error;
        glDeleteProgram(m_programID);
        return false;
    }

    use();

    // Définition des constantes 
    setUniform("gamma",Config::gamma);
    setUniform("blinn",false);

    // Linkage OK
    return true;
}



bool Shader::compile(GLuint &shader, GLenum type, string const &fileSrc)
{
    // Création du shader
    shader = glCreateShader(type);

    // Vérification du shader
    if(shader == 0)
    {
        cout << "Erreur, le type de shader (" << type << ") n'existe pas" << endl;
        return false;
    }

    // Ouverture fichier code source
    ifstream file(fileSrc.c_str());

    // Test d'ouverture du fichier
    if(!file)
    {
        cout << "Erreur : Le fichier " << fileSrc << " est introuvable." << endl;
        glDeleteShader(shader);
        return false;
    }

    // Lecture du code source
    string line;
    string source;
    while(getline(file, line))
        source += line + '\n';

    file.close();

    // Envoi du code source au shader
    const char* sourceStr = source.c_str();
    glShaderSource(shader, 1, &sourceStr, NULL);

    // Compilation du shader
    glCompileShader(shader);

    // Vérification de la compilation
    GLint errorComp;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &errorComp);

    // Erreur lors de la compilation du shader
    if(errorComp != GL_TRUE)
    {
        // Récupération de la taille de l'erreur
        GLint errorLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorLength);

        // Récupération de l'erreur
        char* error = new char[errorLength + 1];
        glGetShaderInfoLog(shader, errorLength, &errorLength, error);
        error[errorLength] = '\0';

        // Affichage de l'erreur
        cout << "SHADER COMPILATION ERROR (" << fileSrc << ") : " << endl <<  error;
        delete[] error;
        glDeleteShader(shader);
        return false;
    }

    // Compilation OK
    return true;
}



Shader& Shader::operator=(Shader const &s)
{
    // Copie des fichiers sources
    m_vertexSrc = s.m_vertexSrc;
    m_fragmentSrc = s.m_fragmentSrc;

    // Chargement du nouveau shader
    load();

    // Retour du pointeur this
    return *this;
}



void Shader::setUniform(string name, int i) const
{
    glUniform1i(glGetUniformLocation(m_programID, name.c_str()), i);
}


void Shader::setUniform(string name, float f) const
{
    glUniform1f(glGetUniformLocation(m_programID, name.c_str()), f);
}


void Shader::setUniform(string name, const vec3& vec) const
{
    glUniform3fv(glGetUniformLocation(m_programID, name.c_str()), 1, value_ptr(vec));
}


void Shader::setUniform(string name, const vec4& vec) const
{
    glUniform4fv(glGetUniformLocation(m_programID, name.c_str()), 1, value_ptr(vec));
}


void Shader::setUniform(string name, const mat4& mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(m_programID, name.c_str()), 1, GL_FALSE, value_ptr(mat));
}


void Shader::setUniform(const Material* mat)
{
    // Si le material est déjà chargé on ne fais rien
    // if(currentMaterial == mat)
    //     return;

    // Components
    glUniform3fv(glGetUniformLocation(m_programID, "material.ambient"),  1, value_ptr(mat->ambient));
    glUniform3fv(glGetUniformLocation(m_programID, "material.diffuse"),  1, value_ptr(mat->diffuse));
    glUniform3fv(glGetUniformLocation(m_programID, "material.specular"), 1, value_ptr(mat->specular));
    
    // Shininess
    glUniform1f (glGetUniformLocation(m_programID, "material.shininess"), mat->shininess);

    // Mise à jour du material courant
    currentMaterial = (Material*)mat;
}



}