#pragma once

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GL/glew.h>

using namespace std;

namespace engine
{
	class CubeMap
	{
		private : 

			GLuint m_id;
			string m_filename;

			bool load();


		public :

			CubeMap(string filename);

			inline GLuint getID() const {return m_id;}

	};
}