#include "texture.h"

namespace engine
{



Texture::Texture(string filename, bool generateMipmap)
:m_filename(filename)
{
	load(generateMipmap);
}


Texture::~Texture()
{

}


bool Texture::load(bool generateMipmap = false)
{
	// Chargement de l'image dans une surface SDL
    SDL_Surface *image = IMG_Load(m_filename.c_str());
    if(image == 0)
    {
        cerr << "Erreur : " << SDL_GetError() << endl;
        return false;
    }

    // Génération de l'ID
    glGenTextures(1, &m_id);

    // Verrouillage
    glBindTexture(GL_TEXTURE_2D, m_id);

    GLenum formatInternal(0);
    GLenum format(0);

    // Image sans transparence
	if(image->format->BytesPerPixel == 3)
	{
		formatInternal = GL_RGB;

		// Format
	    if(image->format->Rmask == 0xff)
	    	format = GL_RGB;
	    else
	    	format = GL_BGR;
	}

	// Image avec transparence
	else if(image->format->BytesPerPixel == 4)
	{
		formatInternal = GL_RGBA;

		// Format
	    if(image->format->Rmask == 0xff)
	    	format = GL_RGBA;
	    else
	    	format = GL_BGRA;
	}

	// Dans les autres cas, on arrête le chargement
	else
	{
	    cerr << "Erreur, format de l'image inconnu" << endl;
	    SDL_FreeSurface(image);
	    return false;
	}

	// Copie des pixels
	glTexImage2D(GL_TEXTURE_2D, 0, formatInternal, image->w, image->h, 0, format, GL_UNSIGNED_BYTE, image->pixels);
	if(generateMipmap)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	}

  	// Application des filtres
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Clamp
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Déverrouillage
	glBindTexture(GL_TEXTURE_2D, 0);

	// Fin de la méthode
	SDL_FreeSurface(image);
	return true;
}


}
