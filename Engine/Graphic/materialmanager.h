#pragma once

#include <map>

using namespace std;


namespace engine
{
	class Material;
	
	class MaterialManager
	{
		private :

			MaterialManager(); 

	    	static map<string,Material*> materials;

	    public :

	    	static void initialize();
	    	static Material* getMaterial(string id);
	};
}