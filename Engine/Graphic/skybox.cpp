#include "skybox.h"

namespace engine
{


SkyBox::SkyBox()
:cubemap(NULL),vbo(0),vao(0)
{
	
}


SkyBox::~SkyBox()
{
    if(cubemap != NULL)
        delete cubemap;
}


bool SkyBox::isLoaded() const
{
    return (cubemap != NULL); 
}


void SkyBox::load(string filename)
{
    if(isLoaded())
        delete cubemap;

    cubemap = new CubeMap(filename);
}



void SkyBox::start()
{
    // Points du cube
    vector<ivec3> vertices;

    //
    //     0------1
    //   2/-----3/|
    //   | |    | |
    //   |/4----|/5
    //   6------7
    //

    float d = 10;

    // 0 1 2
    vertices.push_back(ivec3(-d, d,-d));
    vertices.push_back(ivec3( d, d,-d));
    vertices.push_back(ivec3(-d, d, d));

    // 1 3 2
    vertices.push_back(ivec3( d, d,-d));
    vertices.push_back(ivec3( d, d, d));
    vertices.push_back(ivec3(-d, d, d));

    // 4 6 7
    vertices.push_back(ivec3(-d,-d,-d));
    vertices.push_back(ivec3(-d,-d, d));
    vertices.push_back(ivec3( d,-d, d));

    // 4 7 5
    vertices.push_back(ivec3(-d,-d,-d));
    vertices.push_back(ivec3( d,-d, d));
    vertices.push_back(ivec3( d,-d,-d));

    // 3 6 2
    vertices.push_back(ivec3( d, d, d));
    vertices.push_back(ivec3(-d,-d, d));
    vertices.push_back(ivec3(-d, d, d));

    // 3 7 6
    vertices.push_back(ivec3( d, d, d));
    vertices.push_back(ivec3( d,-d, d));
    vertices.push_back(ivec3(-d,-d, d));

    // 0 4 1
    vertices.push_back(ivec3(-d, d,-d));
    vertices.push_back(ivec3(-d,-d,-d));
    vertices.push_back(ivec3( d, d,-d));

    // 1 4 5
    vertices.push_back(ivec3( d, d,-d));
    vertices.push_back(ivec3(-d,-d,-d));
    vertices.push_back(ivec3( d,-d,-d));

    // 1 7 3
    vertices.push_back(ivec3( d, d,-d));
    vertices.push_back(ivec3( d,-d, d));
    vertices.push_back(ivec3( d, d, d));

    // 1 5 7
    vertices.push_back(ivec3( d, d,-d));
    vertices.push_back(ivec3( d,-d,-d));
    vertices.push_back(ivec3( d,-d, d));

    // 0 2 4
    vertices.push_back(ivec3(-d, d,-d));
    vertices.push_back(ivec3(-d, d, d));
    vertices.push_back(ivec3(-d,-d,-d));

    // 4 2 6
    vertices.push_back(ivec3(-d,-d,-d));
    vertices.push_back(ivec3(-d, d, d));
    vertices.push_back(ivec3(-d,-d, d));

    nbVertices = vertices.size();


    // === VBO ================

    if(glIsBuffer(vbo) == GL_TRUE)
        glDeleteBuffers(1, &vbo);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    int verticesBytesSize = vertices.size() * sizeof(ivec3); 

    glBufferData(GL_ARRAY_BUFFER, verticesBytesSize, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, verticesBytesSize, vertices.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);


    // === VAO ================

    // Destruction d'un éventuel ancien VAO
    if(glIsVertexArray(vao) == GL_TRUE)
        glDeleteVertexArrays(1, &vao);

    // Chargement VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER,vbo);

    Shader* shader = ShaderManager::getShader(ShaderManager::CUBEMAP);

    // Accès aux vertices dans la mémoire vidéo
    GLint posAttrib = glGetAttribLocation(shader->getProgram(), "position");
    glVertexAttribIPointer(posAttrib, 3, GL_INT, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(posAttrib);

    // Déverrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage du VAO
    glBindVertexArray(0);
}



void SkyBox::render() const
{
    if(!isLoaded())
        return;

    // glDepthMask(GL_FALSE);

    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap->getID());

    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, nbVertices);
    glBindVertexArray(0);

    // glDepthMask(GL_TRUE);
}

}