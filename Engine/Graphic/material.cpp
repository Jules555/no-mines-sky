#include "material.h"
#include "shader.h"
#include "shadermanager.h"

namespace engine
{




Material::Material(vec3 a, vec3 d, vec3 s, float sh)
:ambient(a), diffuse(d), specular(s), shininess(sh)
{
	shader = ShaderManager::getShader(ShaderManager::BLINN_PHONG);
}


Material::Material(const Material& m)
:shader(m.shader), ambient(m.ambient), diffuse(m.diffuse), specular(m.specular), shininess(m.shininess)
{
}


bool Material::haveShader(Shader* current) const
{
	return (shader == current);
}

}