#pragma once

#include <iostream>
#include <GL/glew.h>

#include "Utility/glm.h"
#include "materialmanager.h"

using namespace std;
using namespace glm;

namespace engine
{
	class Shader;
	
	class Material
	{

	public:

	    Shader* shader;

	    vec3 ambient;
	    vec3 diffuse;
	    vec3 specular;

	    float shininess;

	    Material(vec3 a, vec3 d, vec3 s, float sh);
	    Material(const Material& m);

	    bool haveShader(Shader*) const;

	};
}
