#pragma once

#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <SDL2/SDL.h>

#include "shadermanager.h"
#include "Config/config.h"
#include "Utility/glm.h"
#include "Graphic/material.h"

using namespace std;


namespace engine
{
	class Material;
	
	class Shader
	{
		private:

		    GLuint m_vertexID;
		    GLuint m_fragmentID;
		    GLuint m_programID;

		    string m_vertexSrc;
	    	string m_fragmentSrc;

	    	Material* currentMaterial = NULL;

	    public :

		    // Shader();
		    Shader(string vertexSrc, string fragmentSrc);
		    Shader(Shader const &s);
		    ~Shader();

		    Shader& operator=(Shader const &s);

		    void use();
		    
		    bool load();
		    bool compile(GLuint &shader, GLenum type, string const &fichierSource);

		    GLuint getProgram() const {return m_programID;}

		    void setUniform(string name, int i) const;
		    void setUniform(string name, float f) const;
		    void setUniform(string name, const vec3& vec) const;
		    void setUniform(string name, const vec4& vec) const;
		    void setUniform(string name, const mat4& mat) const;
		    void setUniform(const Material* mat);
	};
}