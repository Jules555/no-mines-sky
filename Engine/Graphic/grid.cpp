#include "grid.h"

namespace engine
{



Grid::Grid(int s, float i)
:size(s),interval(i)
{

}



void Grid::render()
{
    glPushMatrix();
    glLoadIdentity();

    glDisable(GL_LIGHTING);
    glColor4ub(180,180,180,128);

    float total = size*interval;
    float coord = total/2;

    for(int i=0; i<=size; ++i)
    {
        float delta = i*interval-coord;

        glBegin(GL_LINES);

            glVertex3f(delta,0.0f,-coord);
            glVertex3f(delta,0.0f,coord);

            glVertex3f(-coord,0.0f,delta);
            glVertex3f(coord,0.0f,delta);

        glEnd();
    }

    glEnable(GL_LIGHTING);
    glPopMatrix();
}




}