#include "materialmanager.h"
#include "material.h"

namespace engine
{




map<string,Material*> MaterialManager::materials;


void MaterialManager::initialize()
{
	vec3 _ambient  = vec3(0.1f, 0.1f, 0.1f);
	vec3 _diffuse  = vec3(0.8f, 0.8f, 0.8f);
	vec3 _specular = vec3(0.2f, 0.2f, 0.2f);
	float _shininess = 40.0f;
	materials["Default"] = new Material(_ambient,_diffuse,_specular,_shininess);
}
    	

Material* MaterialManager::getMaterial(string id)
{
	return materials[id];
}


}