#pragma once

#include <vector>

#include "shader.h"
#include "Config/config.h"

using namespace std;


namespace engine
{
    class Shader;
    
    class ShaderManager
    {
    	public :

    		enum Shaders {BLINN_PHONG, CEL_SHADING, TEXTURED, MINECRAFT, CUBEMAP, COUNT};

    	private :

    		ShaderManager(); // uninstanciable

        	static Shader* shaders[ShaderManager::COUNT];
    		static Shader* current;
        	static vector<Shader*> actives;

        	static string getFilename(ShaderManager::Shaders shader);

        public :

        	static Shader* getShader(ShaderManager::Shaders shader);

            static Shader* getCurrentShader();
        	static const vector<Shader*>& getActiveShaders();

            static void setCurrentShader(Shader*);
    };
}