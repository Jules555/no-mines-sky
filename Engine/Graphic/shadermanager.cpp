#include "shadermanager.h"

namespace engine
{




Shader* ShaderManager::shaders[ShaderManager::COUNT] = {NULL};
Shader* ShaderManager::current;
vector<Shader*> ShaderManager::actives;



Shader* ShaderManager::getShader(ShaderManager::Shaders shader)
{
	if(shaders[shader] != NULL)
		return shaders[shader];

	string filename = getFilename(shader);
	if(filename.empty())
		return NULL;

	shaders[shader] = new Shader(Config::PATH_SHADERS+"/"+filename+".vert",Config::PATH_SHADERS+"/"+filename+".frag");
	actives.push_back(shaders[shader]);
	return shaders[shader];
}



string ShaderManager::getFilename(ShaderManager::Shaders shader)
{
	switch(shader)
	{
		case BLINN_PHONG : return "blinn_phong";
		case CEL_SHADING : return "cel_shading";
		case TEXTURED    : return "textured";
		case MINECRAFT   : return "minecraft";
		case CUBEMAP     : return "cubemap";

		default : return "";
	}
}



Shader* ShaderManager::getCurrentShader()
{
	return current;
}

const vector<Shader*>& ShaderManager::getActiveShaders()
{
	return actives;
}

void ShaderManager::setCurrentShader(Shader* _current)
{
	current = _current;
}


}