#pragma once

#include <GL/glew.h>
#include <fstream>


namespace engine
{
	class Grid
	{

		private:
		    int size;
		    float interval;

		public:
		    Grid(int s, float i);

		    void render();

	};
}