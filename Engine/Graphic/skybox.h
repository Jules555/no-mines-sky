#pragma once

#include <iostream>
#include <vector>

#include "Config/config.h"
#include "Utility/glm.h"
#include "shadermanager.h"
#include "cubemap.h"

namespace engine
{
	class SkyBox
	{
		private : 

			CubeMap* cubemap;

			GLuint vbo;
			GLuint vao;

			uint nbVertices;


		public :

			SkyBox();
			~SkyBox();

			bool isLoaded() const;

			void load(string filename);
			void start();
			void render() const;
	};
}