#pragma once

#include <iostream>
#include <cstdlib>

using namespace std;

class Random
{

private:

    Random();

public :

	static void initialize();

	static bool getBool();
    static int getInt(int inf, int sup);
    static float getFloat(float inf, float sup);


};
