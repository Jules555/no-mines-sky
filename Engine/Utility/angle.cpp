#include "angle.h"

float Angle::toRadians(float degrees)
{
	return degrees*M_PI/180;
}

float Angle::toDegrees(float radians)
{
	return radians*180/M_PI;
}
