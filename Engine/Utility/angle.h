#pragma once

#include <cmath>

class Angle
{
	private :

		Angle();

	public :

		static float toRadians(float degrees);
		static float toDegrees(float radians);
};
