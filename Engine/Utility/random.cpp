#include "random.h"

void Random::initialize()
{
	srand(time(NULL));
}


bool Random::getBool()
{
	return bool(getInt(0,1));
}

int Random::getInt(int inf, int sup)
{
	if(inf == sup)
		return inf;

    return rand()%(sup-inf+1)+inf;
}


float Random::getFloat(float inf, float sup)
{
	if(inf == sup)
		return inf;
	
    return float(rand()%int((sup-inf+1)*1000)+inf*1000)/1000;
}


