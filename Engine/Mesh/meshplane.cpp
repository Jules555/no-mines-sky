﻿#include "meshplane.h"

namespace engine
{




MeshPlane::MeshPlane(GameObject* go)
:Mesh(go)
{

}



MeshPlane::MeshPlane(GameObject* go, float w, float h, unsigned int r, unsigned int c)
:Mesh(go), width(w), height(h), cols(c), rows(r)
{
    build();
}


MeshPlane *MeshPlane::clone() const
{
    return new MeshPlane(*this);
}


void MeshPlane::build()
{
    float intervalX = width/cols;
    float intervalZ = height/rows;
    float midX = width/2;
    float midZ = height/2;

    // Calcul des points de la grille
    for(unsigned int j=0; j<=rows; ++j)
        for(unsigned int i=0; i<=cols; ++i)
        {
            vec3 point;
            point.x = (float)i*intervalX-midX;
            point.y = 0;
            point.z = (float)j*intervalZ-midZ;
            vertices.push_back(point);
        }

    // Calcul des triangles
    for(unsigned int j=0; j<rows; ++j)
        for(unsigned int i=0; i<cols; ++i)
        {
            triangles.push_back(Triangle(i+j*(cols+1), i+(j+1)*(cols+1), (i+1)+(j+1)*(cols+1)));
            triangles.push_back(Triangle(i+j*(cols+1), (i+1)+(j+1)*(cols+1), (i+1)+j*(cols+1)));
        };
}


void MeshPlane::applyTexture()
{
    texels.clear();
    float dx = (float)1/cols;
    float dy = (float)1/rows;

    for(unsigned int j=0; j<=rows; ++j)
        for(unsigned int i=0; i<=cols; ++i)
            texels.push_back(vec2(dx*i,dy*j));
}



// void MeshPlane::save(QJsonObject& json) const
// {
//     Mesh::save(json);

//     json["class"] = "MeshPlane";
//     json["width"] = width;
//     json["height"] = height;
//     json["cols"] = (int)cols;
//     json["rows"] = (int)rows;
// }


}