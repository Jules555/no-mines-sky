#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <GL/glew.h>
#include <SDL2/SDL_image.h>

#include "Graphic/graphic.h"
#include "System/component.h"
#include "Utility/glm.h"

using namespace std;
using namespace glm;

namespace engine
{
    struct Triangle
    {
        GLushort x;
        GLushort y;
        GLushort z;

        Triangle()
        {
            x = y = z = 0;
        }

        Triangle(GLushort _p1, GLushort _p2, GLushort _p3)
        {
            x = _p1;
            y = _p2;
            z = _p3;
        }
    };


    class Mesh : public Component
    {

        protected :

            // Attributes
            bool flat;

            vector<vec3> vertices;
            vector<Triangle> triangles;
            vector<vec3> normals;
            vector<vec2> texels;

            const Texture* texture = NULL;

            // For OpenGL rendering
            GLuint vbo;
            GLuint ebo;
            GLuint vao;

            // Methods
            void computeNormals();

            virtual void build() = 0;

            virtual void subscribe();


        public :

            // Attributes
            const Material* material;

            // Methods
            Mesh(GameObject* go);
            virtual ~Mesh();

            virtual Mesh* clone() const = 0;

            virtual void start();
            virtual void render() const;

            void setTexture(const Texture* tex) {texture = tex; applyTexture();}
            virtual void applyTexture() {}

            //virtual void save(QJsonObject &) const;
    };
}