#pragma once

#include "mesh.h"

namespace engine
{
	class MeshCube : public Mesh
	{

	private:
	    float width;

	protected:
	    virtual void build();

	public:
	    MeshCube(GameObject* go, float w);

	    virtual MeshCube* clone() const;
	    
	    //virtual void save(QJsonObject&) const;



	};
}