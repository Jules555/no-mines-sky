#include "meshcube.h"

namespace engine
{



MeshCube::MeshCube(GameObject* go, float w)
:Mesh(go),width(w)
{
    flat = true;
    build();
}

MeshCube *MeshCube::clone() const
{
    return new MeshCube(*this);
}


void MeshCube::build()
{
    float d = width/2;

    // Construction des sommets
    //
    //     0------1
    //   2/-----3/|
    //   | |    | |
    //   |/4----|/5
    //   6------7
    //

    // On veut un rendu "flat", on doit donc dupliquer les points...

    // 0 2 1
    vertices.push_back({-d, d,-d});
    vertices.push_back({-d, d, d});
    vertices.push_back({ d, d,-d});

    // 1 2 3
    vertices.push_back({ d, d,-d});
    vertices.push_back({-d, d, d});
    vertices.push_back({ d, d, d});

    // 4 7 6
    vertices.push_back({-d,-d,-d});
    vertices.push_back({ d,-d, d});
    vertices.push_back({-d,-d, d});

    // 4 5 7
    vertices.push_back({-d,-d,-d});
    vertices.push_back({ d,-d,-d});
    vertices.push_back({ d,-d, d});

    // 3 2 6
    vertices.push_back({ d, d, d});
    vertices.push_back({-d, d, d});
    vertices.push_back({-d,-d, d});

    // 3 6 7
    vertices.push_back({ d, d, d});
    vertices.push_back({-d,-d, d});
    vertices.push_back({ d,-d, d});

    // 0 1 4
    vertices.push_back({-d, d,-d});
    vertices.push_back({ d, d,-d});
    vertices.push_back({-d,-d,-d});

    // 1 5 4
    vertices.push_back({ d, d,-d});
    vertices.push_back({ d,-d,-d});
    vertices.push_back({-d,-d,-d});

    // 1 3 7
    vertices.push_back({ d, d,-d});
    vertices.push_back({ d, d, d});
    vertices.push_back({ d,-d, d});

    // 1 7 5
    vertices.push_back({ d, d,-d});
    vertices.push_back({ d,-d, d});
    vertices.push_back({ d,-d,-d});

    // 0 4 2
    vertices.push_back({-d, d,-d});
    vertices.push_back({-d,-d,-d});
    vertices.push_back({-d, d, d});

    // 4 6 2
    vertices.push_back({-d,-d,-d});
    vertices.push_back({-d,-d, d});
    vertices.push_back({-d, d, d});
}


// void MeshCube::save(QJsonObject& json) const
// {
//     Mesh::save(json);

//     json["class"] = "MeshCube";
//     json["width"] = width;
// }


}