#include "meshsphere.h"

namespace engine
{




MeshSphere::MeshSphere(GameObject* go, float r)
:Mesh(go),radius(r)
{
    build();
}

MeshSphere::MeshSphere(GameObject* go, float r, unsigned int nb_m, unsigned int nb_p)
:Mesh(go),radius(r),meridians(nb_m),parallels(nb_p)
{
    build();
}

MeshSphere *MeshSphere::clone() const
{
    return new MeshSphere(*this);
}

void MeshSphere::build()
{
    // Création points
    for(unsigned int j=1; j<parallels; ++j)
    {
        float theta = j*M_PI/parallels - M_PI/2;
        for(unsigned int i=0; i<meridians; ++i)
        {
            float phi = i*M_PI*2/meridians - M_PI;
            vertices.push_back(vec3(radius*cos(theta)*cos(phi), radius*cos(theta)*sin(phi), radius*sin(theta)));
        }
    }

    vertices.push_back(vec3(0,0,-radius));
    vertices.push_back(vec3(0,0,radius));

    // Création triangles
    for(unsigned int j=0; j<parallels-2; ++j)
    {
        for(unsigned int i=0; i<meridians; ++i)
        {
            uint ip = i < meridians-1 ? i+1 : 0;

            triangles.push_back(Triangle(i+j*meridians, ip+j*meridians, ip+(j+1)*meridians));
            triangles.push_back(Triangle(i+j*meridians, ip+(j+1)*meridians, i+(j+1)*meridians));
        }
    }

    // "Fermeture" de la sphere (haut et bas)
    for(unsigned int i=0; i<meridians; ++i)
    {
        uint ip = i < meridians-1 ? i+1 : 0;

        triangles.push_back(Triangle(i, (parallels-1)*meridians, ip));
        triangles.push_back(Triangle(i+(parallels-2)*meridians, ip+(parallels-2)*meridians, (parallels-1)*meridians+1));
    }
}


// void MeshSphere::save(QJsonObject& json) const
// {
//     Mesh::save(json);

//     json["type"] = "MeshSphere";
//     json["radius"] = radius;
//     json["meridians"] = (int)meridians;
//     json["parallels"] = (int)parallels;
// }


}