#include "meshterrain.h"

namespace engine
{




MeshTerrain::MeshTerrain(GameObject* go, float w, float h, unsigned int r, unsigned int c, bool random, float f)
:MeshPlane(go)
{
    width = w;
    height = h;
    rows = r;
    cols = c;
    factor = f;

    if(random)
        type = TerrainType::RANDOM;
    else
        type = TerrainType::FLAT;

    build();
}


MeshTerrain::MeshTerrain(GameObject* go, float w, float h, string filename, float factor)
:MeshPlane(go),filename(filename),factor(factor)
{
    width = w;
    height = h;

    image = IMG_Load(filename.c_str());
    if(image == NULL)
    {
        cout << "Image invalide ou introuvable." << endl;
        return;
    }

    type = TerrainType::HEIGHTMAP;

    cols = image->w-1;
    rows = image->h-1;

    build();
}


MeshTerrain *MeshTerrain::clone() const
{
    return new MeshTerrain(*this);
}



void MeshTerrain::build()
{
    MeshPlane::build();

    switch(type)
    {
        case TerrainType::HEIGHTMAP :
            applyHeightMap();
            break;

        case TerrainType::RANDOM :
            randomizeHeight();
            break;

        default :
            break;
    }
}



void MeshTerrain::applyHeightMap()
{
    SDL_LockSurface(image);

    Uint32* pixels = (Uint32*)(image->pixels);
    SDL_PixelFormat* format = image->format;
    Uint8 r,g,b,a;

    for(int j=0; j<image->h; j++)
        for(int i=0; i<image->w; i++)
        {
            SDL_GetRGBA(pixels[i+j*image->w],format,&r,&g,&b,&a);

            vertices[i+j*image->w].y = float(r*factor/100);
        }

    SDL_UnlockSurface(image);
}



void MeshTerrain::randomizeHeight()
{
    for(unsigned int j=0; j<=rows; ++j)
        for(unsigned int i=0; i<=cols; ++i)
        {
            // On calcule l'altitude moyenne des points voisins déjà définis
            float moy = 0;
            unsigned int cpt = 0;

            for(int y=-1; y<=1; ++y)
                for(int x=-1; x<=1; ++x)
                {
                    if(x==0 && y==0)
                        break;

                    int ix = i+x;
                    int jy = j+y;

                    if(ix < 0 || ix > (int)cols || jy < 0 || jy > (int)rows)
                        break;

                    if(vertices[ix+jy*(cols+1)].y == 0)
                        break;

                    moy += vertices[ix+jy*(cols+1)].y;
                    cpt++;
                }

            if(cpt > 0)
                moy = moy/cpt;

            vec3& point = vertices[i+(j*(cols+1))];
            float rando = (float)(rand()%200-100)*1/100*factor;
            float coeff = (float)(rand()%100)/100;
            point.y = moy+coeff*rando;
        }

}




// void MeshTerrain::save(QJsonObject& json) const
// {
//     MeshPlane::save(json);

//     json["type"] = "MeshTerrain";
//     json["terrain"] = type;

//     if(type == TerrainType::HEIGHTMAP)
//     {
//         // Modifier la classe, il faut qu'on garde le nom du fichier pour l'enregistrer ici
//     }

//     else if(type == TerrainType::RANDOM)
//     {
//         QJsonArray array;
//         for(int i=0; i<vertices.size(); ++i)
//         {
//             QJsonObject obj;
//             obj["x"] = vertices[i].x();
//             obj["y"] = vertices[i].y();
//             obj["z"] = vertices[i].z();
//             array.append(obj);
//         }
//         json["vertices"] = array;
//     }
// }


}