#pragma once

#include "mesh.h"

namespace engine
{
	class MeshPlane : public Mesh
	{

	protected:

	    float width, height;
	    unsigned int cols, rows;

	public:

	    MeshPlane(GameObject* go);
	    MeshPlane(GameObject* go, float w, float h, unsigned int r, unsigned int c);

	    virtual MeshPlane* clone() const;
	    // virtual void save(QJsonObject &) const;

	protected:

	    virtual void build();

	    virtual void applyTexture();

	};
}
