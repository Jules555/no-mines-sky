#include "meshfile.h"

namespace engine
{




MeshFile::MeshFile(GameObject* go, string filename, MeshFile::FileType type)
:Mesh(go),filename(filename),filetype(type)
{
    build();
}

MeshFile *MeshFile::clone() const
{
    return new MeshFile(*this);
}

void MeshFile::build()
{
    switch(filetype)
    {
        case FileType::OFF :
            readFileOFF(filename);
            break;
    }
}


void MeshFile::readFileOFF(const string &filename)
{
    // Ouverture fichier
    ifstream file(filename.c_str(), ios::in | ios::binary);
    file.seekg(0, ios_base::beg);

    if(!file)
    {
        cerr << "Le fichier n'existe pas." << endl;;
        return;
    }

    // Lecture entête
    string str;
    file >> str;
    if(str != "OFF")
    {
        cerr << "Le fichier n'est pas de type OFF." << endl;
        return;
    }

    unsigned int nb_sommets, nb_triangles, nb_aretes;
    file >> nb_sommets >> nb_triangles >> nb_aretes;

    // Lecture sommets
    vertices.clear();
    for(unsigned int i=0; i<nb_sommets; ++i)
    {
        float x,y,z;
        file >> x >> y >> z;
        vertices.push_back(vec3(x,y,z));
    }

    // Lecture triangle
    triangles.clear();
    for(unsigned int i=0; i<nb_triangles; ++i)
    {
        unsigned int p1,p2,p3;
        file >> p1 >> p1 >> p2 >> p3;
        triangles.push_back(Triangle(p1,p2,p3));
    }

    // Fermeture fichier
    file.close();
}



// void MeshFile::save(QJsonObject& json) const
// {
//     Mesh::save(json);

//     json["class"] = "MeshFile";
//     json["file"] = filename;
//     json["type"] = filetype;
// }


}