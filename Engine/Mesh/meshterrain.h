#pragma once

#include <fstream>

#include "meshplane.h"


namespace engine
{
    class MeshTerrain : public MeshPlane
    {

    private:
        enum TerrainType {FLAT, RANDOM, HEIGHTMAP};
        TerrainType type = TerrainType::FLAT;

        string filename;
        float factor;

        SDL_Surface* image;

    public:
        MeshTerrain(GameObject* go, float w, float h, unsigned int r, unsigned int c, bool random=false, float f=1);
        MeshTerrain(GameObject* go, float w, float h, string filename, float factor=1);

        virtual MeshTerrain* clone() const;

        // virtual void save(QJsonObject &) const;

    protected:
        virtual void build();

        void applyHeightMap();
        void randomizeHeight();

    };
}
