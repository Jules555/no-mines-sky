#include "mesh.h"

namespace engine
{




Mesh::Mesh(GameObject* go)
:Component(go),flat(false),vbo(0),ebo(0),vao(0)
{
    subscribe();

    material = MaterialManager::getMaterial("Default");

    vertices.clear();
    triangles.clear();
    normals.clear();
    texels.clear();
}



Mesh::~Mesh()
{
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);

    vertices.clear();
    triangles.clear();
    normals.clear();
    texels.clear();
}


void Mesh::subscribe()
{
    gameObject->getScene()->subscribeRender(this);
}


void Mesh::computeNormals()
{
    normals.clear();

    // On crée une normale pour chaque point
    for(unsigned int i=0; i<vertices.size(); ++i)
        normals.push_back(vec3(0,0,0));

    // FLAT
    if(flat)
    {
        // On calcule une normale pour chaque triangle
        for(unsigned int i=0; i<vertices.size(); i+=3)
        {
            vec3 p1, p2, p3;

            p1 = vertices[i];
            p2 = vertices[i+1];
            p3 = vertices[i+2];

            vec3 normal = glm::normalize(glm::cross(p2-p1,p3-p1));

            normals[i] = normals[i+1] = normals[i+2] = normal;
        }
    }

    // SMOOTH
    else if(!flat)
    {
        for(unsigned int i=0; i<triangles.size(); ++i)
        {
            Triangle triangle = triangles[i];

            vec3 v1 = vertices[triangle.y]-vertices[triangle.x];
            vec3 v2 = vertices[triangle.z]-vertices[triangle.x];
            vec3 normal = glm::normalize(glm::cross(v1,v2));

            normals[triangle.x] += normal;
            normals[triangle.y] += normal;
            normals[triangle.z] += normal;
        }

        for(unsigned int i=0; i<normals.size(); ++i)
            normals[i] = glm::normalize(normals[i]);
    }
}



void Mesh::start()
{
    computeNormals();

    // === VBO ================

    // Destruction d'un éventuel ancien VBO
    if(glIsBuffer(vbo) == GL_TRUE)
        glDeleteBuffers(1, &vbo);

    // Chargement VBO
    int verticesBytesSize = vertices.size() * sizeof(vec3);
    int normalsBytesSize  = normals.size() * sizeof(vec3);
    int texelsBytesSize   = texels.size() * sizeof(vec2); 

    int totalBytesSize = verticesBytesSize + normalsBytesSize;
    if(texture != NULL)
        totalBytesSize += texelsBytesSize;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData(GL_ARRAY_BUFFER, totalBytesSize, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, verticesBytesSize, vertices.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytesSize, normalsBytesSize, normals.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytesSize+normalsBytesSize, texelsBytesSize, texels.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // === EBO ================

    // On ne construit par indice que pour un rendu "smooth"
    if(!flat)
    {
        // Destruction d'un éventuel ancien EBO
        if(glIsBuffer(ebo) == GL_TRUE)
            glDeleteBuffers(1, &ebo);

        // Chargement EBO    
        int indicesBytesSize = triangles.size() * sizeof(Triangle);

        glGenBuffers(1,&ebo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBytesSize, 0, GL_STATIC_DRAW);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indicesBytesSize, triangles.data());
    }

    // === VAO ================

    // Destruction d'un éventuel ancien VAO
    if(glIsVertexArray(vao) == GL_TRUE)
        glDeleteVertexArrays(1, &vao);

    // Chargement VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER,vbo);

    // Verrouillage du EBO
    if(!flat)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

    // Accès aux vertices dans la mémoire vidéo
    GLint posAttrib = glGetAttribLocation(material->shader->getProgram(), "vertex");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(posAttrib);

    // Accès aux normales dans la mémoire vidéo
    GLint normAttrib = glGetAttribLocation(material->shader->getProgram(), "normal");
    glVertexAttribPointer(normAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(verticesBytesSize));
    glEnableVertexAttribArray(normAttrib);

    // Accès à la texture et aux texels dans la mémoire vidéo
    if(texture != NULL)
    {
        glBindTexture(GL_TEXTURE_2D, texture->getID());

        GLint texAttrib = glGetAttribLocation(material->shader->getProgram(), "texel");
        glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(verticesBytesSize+normalsBytesSize));
        glEnableVertexAttribArray(texAttrib);
    }

    // Déverrouillage du EBO
    if(!flat)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Déverrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage du VAO
    glBindVertexArray(0);
}




void Mesh::render() const
{
    // Si le matériau n'est pas compatible avec le shader courant, on ne dessine pas
    if(!material->haveShader(ShaderManager::getCurrentShader()))
        return;

    // Activation du material
    ShaderManager::getCurrentShader()->setUniform(material);

    // Texture ?
    if(texture != NULL)
        glBindTexture(GL_TEXTURE_2D, texture->getID());

    // Envoi de la matrice modelview
    Camera* camera = gameObject->scene->camera;
    mat4 modelview = camera->getLook() * gameObject->transform.getMatrix();
    material->shader->setUniform("modelview",modelview);

    // Dessin
    glBindVertexArray(vao);

    // FLAT
    if(flat)
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());

    // SMOOTH
    else
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glDrawElements(GL_TRIANGLES, triangles.size() * 3, GL_UNSIGNED_SHORT, NULL);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    glBindVertexArray(0);
}




// void Mesh::save(QJsonObject& json) const
// {
//     QJsonObject tra;
//     transform.save(tra);
//     json["transform"] = tra;
// }



}