#pragma once

#include "mesh.h"


namespace engine
{
	class MeshSphere : public Mesh
	{

	private :

	    float radius;
	    unsigned int meridians = 25;
	    unsigned int parallels = 25;

	public:

	    MeshSphere(GameObject* go, float r);
	    MeshSphere(GameObject* go, float r, unsigned int nb_m, unsigned int nb_p);

	    virtual MeshSphere* clone() const;

	    // virtual void save(QJsonObject &) const;

	protected:

	    virtual void build();


	};
}