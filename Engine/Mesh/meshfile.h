#pragma once

#include "mesh.h"


namespace engine
{
	class MeshFile : public Mesh
	{

	public:

	    enum FileType {OFF}; // d'autres à venir peut-être...

	    MeshFile(GameObject* go, string filename, FileType type);

	    virtual MeshFile* clone() const;
	    //virtual void save(QJsonObject &) const;

	protected :

	    virtual void build();

	private:

	    string filename;
	    FileType filetype;

	    void readFileOFF(const string& filename);

	};
}
