#pragma once

#include "Utility/glm.h"
#include "Config/config.h"
#include "Time/time.h"

using namespace glm;

namespace engine
{
    class Mouse
    {


    public :

        enum Key
        {
            LEFT_CLICK, RIGHT_CLICK, MIDDLE_CLICK, WHEEL_DOWN, WHEEL_UP,
            UNKNOW, COUNT
        };


    private :

        Mouse();

        static bool firstMove;


    public:

        static ivec2 position;
        static vec2 delta;

        static void mouseMoved(SDL_MouseMotionEvent event);

        static void initialize();

        static void newFrame();

    };
}