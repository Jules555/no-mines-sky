#include "mouse.h"

namespace engine
{




ivec2 Mouse::position;
vec2 Mouse::delta;
bool Mouse::firstMove = true;


void Mouse::initialize()
{

}


void Mouse::mouseMoved(SDL_MouseMotionEvent event)
{
    if(firstMove)
    {
        firstMove = false;
        return;
    }

    position.x = event.x;
    position.y = event.y;

    delta.x = event.xrel;
    delta.y = event.yrel;

    // Smooth (exponential decay)
    float smooth = 1 - exp(log(0.5) * 50 * Time::getDeltaTime());
    delta *= smooth;

    // On masque et bloque le curseur
    SDL_SetRelativeMouseMode(SDL_TRUE);
}




void Mouse::newFrame()
{
    delta.x = 0;
    delta.y = 0;
}


}