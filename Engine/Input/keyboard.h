#pragma once

#include <vector>
#include <SDL2/SDL.h>

using namespace std;

namespace engine
{
    class Keyboard
    {

    public :

        enum Key
        {
            A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
            SPACE, ALT, ALT_GR, SHIFT, CTRL,
            LEFT, RIGHT, UP, DOWN,
            UNKNOW, COUNT
        };


    private :

        Keyboard();

        static vector<bool> keysPressed;

        static Key convertKeyCode(SDL_Keycode);


    public:

        static void initialize();

        static bool isKeyPressed(Key key);

        static void keyPressed(SDL_Keycode);
        static void keyReleased(SDL_Keycode);

    };
}