#include "keyboard.h"

namespace engine
{




vector<bool> Keyboard::keysPressed;


void Keyboard::initialize()
{
    // Initialisation du tableau
    for(unsigned int i=0; i<int(Key::COUNT); ++i)
        keysPressed.push_back(false);
}


bool Keyboard::isKeyPressed(Key key)
{
    return keysPressed[key];
}


void Keyboard::keyPressed(SDL_Keycode key)
{
    keysPressed[convertKeyCode(key)] = true;
}


void Keyboard::keyReleased(SDL_Keycode key)
{
    keysPressed[convertKeyCode(key)] = false;
}


Keyboard::Key Keyboard::convertKeyCode(SDL_Keycode key)
{
    switch(key)
    {
        case SDLK_a :
        return Key::A;
        break;

        case SDLK_b :
        return Key::B;
        break;

        case SDLK_c :
        return Key::C;
        break;

        case SDLK_d :
        return Key::D;
        break;

        case SDLK_e :
        return Key::E;
        break;

        case SDLK_f :
        return Key::F;
        break;

        case SDLK_g :
        return Key::G;
        break;

        case SDLK_h :
        return Key::H;
        break;

        case SDLK_i :
        return Key::I;
        break;

        case SDLK_j :
        return Key::J;
        break;

        case SDLK_k :
        return Key::K;
        break;

        case SDLK_l :
        return Key::L;
        break;

        case SDLK_m :
        return Key::M;
        break;

        case SDLK_n :
        return Key::N;
        break;

        case SDLK_o :
        return Key::O;
        break;

        case SDLK_p :
        return Key::P;
        break;

        case SDLK_q :
        return Key::Q;
        break;

        case SDLK_r :
        return Key::R;
        break;

        case SDLK_s :
        return Key::S;
        break;

        case SDLK_t :
        return Key::T;
        break;

        case SDLK_u :
        return Key::U;
        break;

        case SDLK_v :
        return Key::V;
        break;

        case SDLK_w :
        return Key::W;
        break;

        case SDLK_x :
        return Key::X;
        break;

        case SDLK_y :
        return Key::Y;
        break;

        case SDLK_z :
        return Key::Z;
        break;

        case SDLK_SPACE :
        return Key::SPACE;
        break;

        case SDLK_LALT :
        return Key::ALT;
        break;

        case SDLK_RALT :
        return Key::ALT_GR;
        break;

        case SDLK_LSHIFT :
        return Key::SHIFT;
        break;

        case SDLK_LCTRL :
        return Key::CTRL;
        break;

        case SDLK_LEFT :
        return Key::LEFT;
        break;

        case SDLK_RIGHT :
        return Key::RIGHT;
        break;

        case SDLK_UP :
        return Key::UP;
        break;

        case SDLK_DOWN :
        return Key::DOWN;
        break;


        default :
            return Key::UNKNOW;
    }
}


}