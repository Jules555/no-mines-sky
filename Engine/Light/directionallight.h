#pragma once

#include "Utility/glm.h"
#include "light.h"

namespace engine
{
	class DirectionalLight : public Light
	{

	protected :

	    vec3 direction = {0.0f, 0.0f, 1.0f};

	public:

	    DirectionalLight(GameObject* go);

	    virtual DirectionalLight* clone() const;

	    virtual void setUniform(const Shader&, string prefix) const;

	    inline void setDirection(vec3 dir) {direction = dir;}
	    inline const vec3& getDirection() const {return direction;}

	};
}