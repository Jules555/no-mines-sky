#pragma once

#include <iostream>
#include <set>

#include "Graphic/shader.h"

using namespace std;


namespace engine
{
	class Light;
	
	class LightManager
	{

		private :

			LightManager();

			static set<Light*> lights;

		public :

			static void addLight(Light*);
			static void removeLight(Light*);

			static void updateLights(Shader*);

	};
}