#include "lightmanager.h"
#include "light.h"

namespace engine
{




set<Light*> LightManager::lights;


void LightManager::addLight(Light* l)
{
	lights.insert(l);
}


void LightManager::removeLight(Light* l)
{
	lights.erase(lights.find(l));
}


void LightManager::updateLights(Shader* shader)
{
	shader->use();
	shader->setUniform("nbLights",int(lights.size()));

	int ind = 0;
	for (set<Light*>::iterator it = lights.begin(); it != lights.end(); ++it)
	{
		string prefix = "lights["+to_string(ind++)+"].";
		(*it)->setUniform(*shader,prefix);
	}
}

}