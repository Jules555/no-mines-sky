#include "directionallight.h"

namespace engine
{



DirectionalLight::DirectionalLight(GameObject *go)
:Light(go)
{

}

DirectionalLight *DirectionalLight::clone() const
{
    return new DirectionalLight(*this);
}

void DirectionalLight::setUniform(const Shader& shader, string prefix) const
{
    Light::setUniform(shader,prefix);

    // Direction de la lumière
    vec3 dir = vec3(gameObject->scene->camera->getLook() * gameObject->transform.getMatrix() * vec4(direction,0.0f));
    shader.setUniform(prefix+"position", vec4(dir,0.0f));
}



}