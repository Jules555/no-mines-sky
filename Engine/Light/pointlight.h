#pragma once

#include "light.h"

namespace engine
{
    class PointLight: public Light
    {

    	protected :

       		float attenuation;

        public:

            PointLight(GameObject* go);
            
            virtual PointLight* clone() const;

        	// Envoi de la lumière à la carte graphique
            virtual void setUniform(const Shader&, string prefix) const;

            // Setters
        	inline void setAttenuation(float a) {attenuation = a;}

        	// Getters
        	inline float getAttenuation() const {return attenuation;}
    };
}