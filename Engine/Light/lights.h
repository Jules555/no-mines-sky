#pragma once

#include "light.h"

#include "directionallight.h"
#include "pointlight.h"
#include "spotlight.h"
