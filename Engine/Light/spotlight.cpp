#include "spotlight.h"

namespace engine
{




SpotLight::SpotLight(GameObject *go)
:PointLight(go)
{

}

SpotLight* SpotLight::clone()const
{
    return new SpotLight(*this);
}


void SpotLight::setUniform(const Shader& shader, string prefix) const
{
    PointLight::setUniform(shader,prefix);

    // Position
    vec3 pos = vec3(gameObject->scene->camera->getLook() * vec4(gameObject->transform.getPosition(),1.0f));
    shader.setUniform(prefix+"position", vec4(pos,2.0f));

    // Cone direction
    vec3 dir = vec3(gameObject->scene->camera->getLook() * gameObject->transform.getMatrix() * vec4(direction,1.0f)) ;
    shader.setUniform(prefix+"coneDirection", dir);

    // Cone angle
    shader.setUniform(prefix+"coneAngle", angle);
}

}