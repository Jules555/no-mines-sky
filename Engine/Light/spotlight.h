#pragma once

#include "pointlight.h"
#include "Utility/angle.h"

namespace engine
{
    class SpotLight : public PointLight
    {

        protected:

            vec3 direction = vec3(0.0, 0.0f, -1.0f);
            float angle = 20.0f;
            float exponent = 60.0f;

        public:

            SpotLight(GameObject* go);

            virtual SpotLight* clone() const;

            // Envoi de la lumière à la carte graphique
            virtual void setUniform(const Shader&, string prefix) const;

            // Getters
            inline vec3 getDirection() const {return direction;}
            inline float getAngle() const {return angle;}
            inline float getExponent() const {return exponent;}

            // Setters
            inline void setDirection(vec3 d) {direction= d;}
            inline void setAngle(float a) {angle = a;}
            inline void setExponent(float e) {exponent = e;}

    };
}

