#pragma once

#include "System/component.h"
#include "Graphic/material.h"
#include "Graphic/shader.h"
#include "lightmanager.h"



namespace engine
{
    class Light : public Component
    {


    protected:

       vec3 intensity; // Couleur de la lumière
       float ambient;

       virtual void subscribe();


    public:

        Light(GameObject* go);
        ~Light();

        virtual Light* clone() const;

        // Envoi de la lumière à la carte graphique
        virtual void setUniform(const Shader&, string prefix) const;

        // Getters
        inline vec3 getIntensity() {return intensity;}
        inline float getAmbient() {return ambient;}

        // Setters
        inline void setIntensity(vec3 i) {intensity = i;}
        inline void setAmbient(float a) {ambient = a;}

        // Sauvegarde
        //virtual void save(QJsonObject &) const;
    };
}
