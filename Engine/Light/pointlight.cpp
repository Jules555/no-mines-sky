#include "pointlight.h"

namespace engine
{





PointLight::PointLight(GameObject* go)
:Light(go)
{
	attenuation = 0.02f;
}

PointLight *PointLight::clone() const
{
    return new PointLight(*this);
}

void PointLight::setUniform(const Shader& shader, string prefix) const
{
	Light::setUniform(shader,prefix);

    // Position de la lumière
    vec3 pos = vec3(gameObject->scene->camera->getLook() * vec4(gameObject->transform.getPosition(),1.0f));
    shader.setUniform(prefix+"position", vec4(pos,1.0f));

    // Attenuation
    shader.setUniform(prefix+"attenuation", attenuation);

    // Cone angle (pour distinguer de la spotlight)
    shader.setUniform(prefix+"coneAngle", 360);
}

}