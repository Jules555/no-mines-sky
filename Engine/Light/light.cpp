#include "light.h"

namespace engine
{





// ====== CONSTRUCTEURS ====== //

Light::Light(GameObject* go)
:Component(go)
{
    subscribe();
    LightManager::addLight(this);

	intensity = vec3(1.0f, 1.0f, 1.0f);
	ambient = 0.02f;
}

Light *Light::clone() const
{
    return new Light(*this);
}

Light::~Light()
{
    LightManager::removeLight(this);
}



void Light::subscribe()
{
    //gameObject->getScene()->subscribeRender(this);
}


void Light::setUniform(const Shader& shader, string prefix) const
{
    // Intensité de la lumière
    shader.setUniform(prefix+"intensity", intensity);

    // Ambient coefficient
    shader.setUniform(prefix+"ambient", ambient);
}





// void Light::save(QJsonObject& json) const
// {
//     json["je c po"] = "Tu le fera Jérémie !";
// }



}