// Version du GLSL
#version 330 core

// Entries
in vec3 vertex;
in vec3 normal;

// Matrices
uniform mat4 projection;
uniform mat4 modelview;

out vec3 fragVertex;
out vec3 fragNormal;
out vec3 fragPosition;

void main() {

    // Pass some variables to the fragment shader
    fragVertex = vertex;
    fragNormal = normal;
    
    // Apply all matrix transformations to vert
    fragPosition = vec3(modelview * vec4(vertex, 1));
    gl_Position = projection * vec4(fragPosition, 1);
}