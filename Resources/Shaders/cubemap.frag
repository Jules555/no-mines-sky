// Version du GLSL
#version 330 core

// Attributes
in vec3 fragTexel;

// Texture
uniform samplerCube cubeMap;

// Output
out vec4 finalColor;


// Main
void main() 
{
    finalColor = texture(cubeMap,fragTexel);
}



