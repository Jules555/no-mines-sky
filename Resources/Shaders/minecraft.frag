// Version du GLSL
#version 330 core

// Attributes
in vec2 fragTexel;
in float fragAdjacent;

// Texture
uniform sampler2D texBuffer;

// Output
out vec4 finalColor;


// Main
void main() 
{
	// Application texture
    finalColor = texture2D(texBuffer,fragTexel,1.0f);
    // finalColor = vec4(1.0f,1.0f,1.0f,1.0f);

    // Occlusion ambiante
    finalColor += vec4( -vec3(0.1f,0.1f,0.1f) * fragAdjacent, 1.0f);
}



