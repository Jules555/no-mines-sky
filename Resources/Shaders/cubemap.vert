// Version du GLSL
#version 330 core

// Entries
in ivec3 position;

// Matrices
uniform mat4 projection;
uniform mat4 modelview;

// Fragment output
out vec3 fragTexel;

void main() 
{
    // Pass some variables to the fragment shader
    fragTexel = position;
    
    // Apply all matrix transformations to vert
    vec4 tmpPosition = projection * modelview * ivec4(position, 1.0f);
    gl_Position = tmpPosition.xyww;
}