// Version du GLSL
#version 330 core

// Light
#define MAX_LIGHTS 8
uniform int nbLights = 0;
uniform struct Light {
   vec4 position;
   vec3 intensity;
   float ambient;
   float attenuation;
   float coneAngle;    
   vec3 coneDirection; 
} lights[MAX_LIGHTS];

// Material
uniform struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular; 
    float shininess;
} material;

// Matrices
uniform mat4 modelview;

// Others
uniform float gamma = 1.0f;

// Attributes
in vec3 fragVertex;
in vec3 fragNormal;
in vec2 fragTexel;
in vec3 fragPosition;

// Texture
uniform sampler2D texBuffer;

// Position de la caméra (test)
vec3 cameraPosition = vec3(0.0f, 0.0f, 0.0f);

// Output
out vec4 finalColor;



// Apply a light
vec3 ApplyLight(Light light, vec3 normal, vec3 surfaceToCamera)
{
    vec3 surfaceToLight; // Vertex to light vector
    float attenuation = 1.0f;

    // Directional light
    if(light.position.w == 0.0f)
        surfaceToLight = normalize(light.position.xyz);

    // Point light
    else
    {
        surfaceToLight = normalize(vec3(light.position.xyz - fragPosition));
        float distanceToLight = length(light.position.xyz - fragPosition);
        attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

        // Spot light
        if(light.coneAngle < 90)
        {
            float lightToSurfaceAngle = degrees(acos(dot(surfaceToLight, normalize(light.coneDirection))));
            if(lightToSurfaceAngle > light.coneAngle)
                attenuation = 0.0;
        }
    }

    // Ambient
    vec3 ambient = light.ambient * material.ambient * light.intensity;

    // Diffuse
    float diffuseCoeff = max(0.0, dot(normal, normalize(surfaceToLight)));
    vec3 diffuse = diffuseCoeff * material.diffuse * light.intensity;

    // Specular (Phong)
    // vec3 incidenceVector = -surfaceToLight; // a unit vector
    // vec3 reflectionVector = reflect(incidenceVector, normal); // also a unit vector
    // float cosAngle = max(0.0, dot(surfaceToCamera, reflectionVector));
    // float specularCoeff = pow(cosAngle, material.shininess);
    // vec3 specular = specularCoeff * material.specular * light.intensity;

    // Specular (Blinn-Phong)
    vec3 halfwayDir = normalize(surfaceToLight + surfaceToCamera);
    float cosAngle = max(0.0, dot(normal, halfwayDir));
    float specularCoeff = pow(cosAngle, material.shininess);
    vec3 specular = specularCoeff * material.specular * light.intensity;

    // Calculate final color of the pixel, based on the three Phong components
    return ambient + attenuation*(diffuse + specular);
}



// Main
void main() 
{
    // Calculate normal in world coordinates
    mat3 normalMatrix = transpose(inverse(mat3(modelview)));
    vec3 normal = normalize(normalMatrix * fragNormal);

    // Calculate point to eye vector
    vec3 surfaceToCamera = normalize(cameraPosition - vec3(fragPosition)); 

    // Apply each lights
    vec3 linearColor = vec3(0);
    for(int i = 0; i<nbLights && i<MAX_LIGHTS; ++i)
        linearColor += ApplyLight(lights[i], normal, surfaceToCamera);

    // Gamma correction
    vec3 gammaVec = vec3(gamma);
    finalColor = vec4(pow(linearColor, gammaVec), 1.0f);
}



