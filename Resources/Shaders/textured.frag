// Version du GLSL
#version 330 core

// Attributes
in vec2 fragTexel;

// Texture
uniform sampler2D texBuffer;

// Output
out vec4 finalColor;


// Main
void main() 
{
    finalColor = texture2D(texBuffer,fragTexel);
    //finalColor = vec4(fragTexel,fragTexel.x,1.0f);
}



