// Version du GLSL
#version 330 core

// Light
#define MAX_LIGHTS 8
uniform int nbLights = 0;
uniform struct Light {
   vec4 position;
   vec3 intensity;
   float ambient;
   float attenuation;
   float coneAngle;    
   vec3 coneDirection; 
} lights[MAX_LIGHTS];

// Material
uniform struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular; 
    float shininess;
} material;

// Matrices
uniform mat4 modelview;

// Others
uniform float gamma = 1.0f;

// Attributes
in vec3 fragVertex;
in vec3 fragNormal;
in vec3 fragPosition;

// Position de la caméra (test)
vec3 cameraPosition = vec3(0.0f, 0.0f, 0.0f);

// Output
out vec4 finalColor;

uniform bool blinn;



// Apply a light
float ApplyLight(Light light, vec3 normal, vec3 surfaceToCamera)
{
    vec3 surfaceToLight; // Vertex to light vector
    float attenuation = 1.0f;

    // Directional light
    if(light.position.w == 0.0f)
        surfaceToLight = normalize(light.position.xyz);

    // Point light
    else
    {
        surfaceToLight = normalize(vec3(light.position.xyz - fragPosition));
        float distanceToLight = length(light.position.xyz - fragPosition);
        attenuation = 1.0 / (1.0 + light.attenuation * pow(distanceToLight, 2));

        // Spot light
        if(light.coneAngle < 90)
        {
            float lightToSurfaceAngle = degrees(acos(dot(surfaceToLight, normalize(light.coneDirection))));
            if(lightToSurfaceAngle > light.coneAngle)
                attenuation = 0.0;
        }
    }

    // Outline ?
    if(dot(surfaceToCamera,normal) < mix(0,1,max(0.4f,dot(normal,-surfaceToLight))))
        return 0.0f;

    // Ambient
    float ambientCoeff = 0.1f;

    // Diffuse
    float diffuseCoeff = clamp(dot(surfaceToLight,normal),0.0f,1.0f);

    // Specular
    vec3 halfwayDir = normalize(surfaceToLight + surfaceToCamera);
    float specularCoeff = 0.0f;
    if(dot(normal,surfaceToLight) > 0)
        specularCoeff = pow(clamp(dot(halfwayDir,normal),0.0f,1.0f),64.0f);

    // Calculate final color of the pixel, based on the three Phong components
    return ambientCoeff + diffuseCoeff + specularCoeff;
}



// Main
void main() 
{
    // Calculate normal in world coordinates
    mat3 normalMatrix = transpose(inverse(mat3(modelview)));
    vec3 normal = normalize(normalMatrix * fragNormal);

    // Calculate point to eye vector
    vec3 surfaceToCamera = normalize(cameraPosition - vec3(fragPosition)); 

    // Apply each lights
    float shade = 0;
    for(int i = 0; i<nbLights && i<MAX_LIGHTS; ++i)
        shade += ApplyLight(lights[i], normal, surfaceToCamera);

    // Gamma correction
    // vec3 gammaVec = vec3(gamma);
    // float shade = pow(linearColor, gammaVec);

    // Toon ?
    shade = ceil(shade*4)/4;
    finalColor = vec4(material.ambient*shade,1.0f);
}



