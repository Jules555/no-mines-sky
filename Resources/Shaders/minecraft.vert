// Version du GLSL
#version 330 core

// Entries
in vec3 vertex;
in vec2 texel;
in float adjacent;

// Matrices
uniform mat4 projection;
uniform mat4 modelview;

// Fragment output
out vec2 fragTexel;
out float fragAdjacent;

void main() {

    // Pass some variables to the fragment shader
    fragTexel = texel;
    fragAdjacent = adjacent;
    
    // Apply all matrix transformations to vert
    gl_Position = projection * modelview * vec4(vertex, 1);
}