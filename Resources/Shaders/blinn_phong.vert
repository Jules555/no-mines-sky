// Version du GLSL
#version 150 core

// Entries
in vec3 vertex;
in vec3 normal;
in vec2 texel;

// Matrices
uniform mat4 projection;
uniform mat4 modelview;

out vec3 fragVertex;
out vec3 fragNormal;
out vec2 fragTexel;
out vec3 fragPosition;

void main() {

    // Pass some variables to the fragment shader
    fragVertex = vertex;
    fragNormal = normal;
    fragTexel = texel;
    
    // Apply all matrix transformations to vert
    fragPosition = vec3(modelview * vec4(vertex, 1));
    gl_Position = projection * vec4(fragPosition, 1);

}