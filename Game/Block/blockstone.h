#pragma once

#include <vector>
#include <Utility/glm.h>
#include <System/gameobject.h>

#include "block.h"

using namespace std;
using namespace engine;


class BlockStone : public Block
{
	friend class Map;

	public :

		BlockStone();
		

	protected : 

		virtual void buildTexels();
};
