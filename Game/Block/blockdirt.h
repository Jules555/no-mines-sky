#pragma once

#include <vector>
#include <Utility/glm.h>
#include <System/gameobject.h>

#include "block.h"

using namespace std;
using namespace engine;


class BlockDirt : public Block
{
	friend class Map;

	public :

		BlockDirt();
		

	protected : 

		virtual void buildTexels();
};
