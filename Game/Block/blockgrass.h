#pragma once

#include <vector>
#include <Utility/glm.h>
#include <System/gameobject.h>

#include "block.h"

using namespace std;
using namespace engine;


class BlockGrass : public Block
{
	friend class Map;

	public :

		BlockGrass();
		

	protected : 

		virtual void buildTexels();
};
