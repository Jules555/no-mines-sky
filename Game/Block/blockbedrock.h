#pragma once

#include <vector>
#include <Utility/glm.h>
#include <System/gameobject.h>

#include "block.h"

using namespace std;
using namespace engine;


class BlockBedRock : public Block
{
	friend class Map;

	public :

		BlockBedRock();
		

	protected : 

		virtual void buildTexels();
};
