#include "block.h"


Block::Block()
{
	buildVertices();
	buildNormals();
}



Block::~Block()
{
    vertices.clear();
    normals.clear();
    texels.clear();
}


void Block::buildVertices()
{
	float d = 0.1f;

    // Construction des sommets
    //
    //     0------1
    //   2/-----3/|
    //   | |    | |
    //   |/6----|/7
    //   4------5
    //

    // 0 2 3
    vertices.push_back(vec3(0, d, 0));
    vertices.push_back(vec3(0, d, d));
    vertices.push_back(vec3(d, d, d));

    // 0 3 1
    vertices.push_back(vec3(0, d, 0));
    vertices.push_back(vec3(d, d, d));
    vertices.push_back(vec3(d, d, 0));

    // 4 6 7
    vertices.push_back(vec3(0, 0, d));
    vertices.push_back(vec3(0, 0, 0));
    vertices.push_back(vec3(d, 0, 0));

    // 4 7 5
    vertices.push_back(vec3(0, 0, d));
    vertices.push_back(vec3(d, 0, 0));
    vertices.push_back(vec3(d, 0, d));

    // 2 4 5
    vertices.push_back(vec3(0, d, d));
    vertices.push_back(vec3(0, 0, d));
    vertices.push_back(vec3(d, 0, d));

    // 2 5 3
    vertices.push_back(vec3(0, d, d));
    vertices.push_back(vec3(d, 0, d));
    vertices.push_back(vec3(d, d, d));

    // 1 7 6
    vertices.push_back(vec3(d, d, 0));
    vertices.push_back(vec3(d, 0, 0));
    vertices.push_back(vec3(0, 0, 0));

    // 1 6 0
    vertices.push_back(vec3(d, d, 0));
    vertices.push_back(vec3(0, 0, 0));
    vertices.push_back(vec3(0, d, 0));

    // 0 6 4
    vertices.push_back(vec3(0, d, 0));
    vertices.push_back(vec3(0, 0, 0));
    vertices.push_back(vec3(0, 0, d));

    // 0 4 2
    vertices.push_back(vec3(0, d, 0));
    vertices.push_back(vec3(0, 0, d));
    vertices.push_back(vec3(0, d, d));

    // 3 5 7
    vertices.push_back(vec3(d, d, d));
    vertices.push_back(vec3(d, 0, d));
    vertices.push_back(vec3(d, 0, 0));

    // 3 7 1
    vertices.push_back(vec3(d, d, d));
    vertices.push_back(vec3(d, 0, 0));
    vertices.push_back(vec3(d, d, 0));
}


void Block::buildNormals()
{
    // On calcule une normale pour chaque triangle
    for(unsigned int i=0; i<vertices.size(); i+=3)
    {
        vec3 p1, p2, p3;

        p1 = vertices[i];
        p2 = vertices[i+1];
        p3 = vertices[i+2];

        vec3 normal = glm::normalize(glm::cross(p2-p1,p3-p1));

        normals.push_back(normal);
        normals.push_back(normal);
        normals.push_back(normal);
    }
}



uint Block::getIndex(Faces face) const
{
    switch(face)
    {
        case TOP    : return 0;
        case BOTTOM : return 6;
        case FRONT  : return 12;
        case BACK   : return 18;
        case LEFT   : return 24;
        case RIGHT  : return 30;
    }

    return 0;
}