#include "blocks.h"
#include "block.h"

Block* Blocks::buildBlock(BlockType type)
{
	switch(type)
	{
		case BEDROCK : return new BlockBedRock();
		case GRASS   : return new BlockGrass();
		case DIRT    : return new BlockDirt();
		case STONE   : return new BlockStone();

		default : return NULL;
	}
}