#include "blockgrass.h"


BlockGrass::BlockGrass()
:Block()
{
    buildTexels();
}



void BlockGrass::buildTexels()
{
	texels.clear();

	float dx, dy;
	dx = dy = 1.0f/16;

	// Construction des sommets
    //
    //     0------1
    //   2/-----3/|
    //   | |    | |
    //   |/6----|/7
    //   4------5
    //

    int x = 7;
	int y = 13;

	// 0 2 3
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

	// 0 3 1
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));

	x = 2;
	y = 0;

    // 4 6 7
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

    // 4 7 5
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));
	

	x = 3;
	y = 0;

    // 2 4 5
    texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

    // 2 5 3
    texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));

    // 1 7 6
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

    // 1 6 0
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));

    // 0 6 4
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

    // 0 4 2
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));

    // 3 5 7
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*x    , dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));

    // 3 5 1
	texels.push_back(vec2(dx*x    , dy*y));
	texels.push_back(vec2(dx*(x+1), dy*(y+1)));
	texels.push_back(vec2(dx*(x+1), dy*y));
}
