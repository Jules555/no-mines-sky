#pragma once

#include "block.h"
#include "blockbedrock.h"
#include "blockgrass.h"
#include "blockdirt.h"
#include "blockstone.h"

class Block;

/** 
* Block factory
*/

class Blocks 
{
	private :
		Blocks(); // Uninstanciable

	public :
		enum BlockType {NONE, BEDROCK, GRASS, DIRT, STONE, COUNT};
		static Block* buildBlock(BlockType);
};

