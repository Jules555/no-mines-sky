#pragma once

#include <vector>
#include <Utility/glm.h>
#include <System/gameobject.h>

using namespace std;
using namespace engine;


class Block
{
	friend class Map;
	friend class Chunk;

	public :

		enum Faces {TOP, BOTTOM, FRONT, BACK, LEFT, RIGHT};

		Block();
		virtual ~Block();

	protected : 

		vector<vec3> vertices;
		vector<vec3> normals;
		vector<vec2> texels;

		void buildVertices();
		void buildNormals();
		virtual void buildTexels() = 0;

		uint getIndex(Faces) const;
};
