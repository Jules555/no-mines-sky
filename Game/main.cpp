#include <SDL2/SDL.h>
#include <gamewindow.h>

#include "Block/blocks.h"
#include "Map/minemap.h"

using namespace std;
using namespace engine;
using namespace noise;


int main(int argc, char **argv)
{

    // ====================
    //  Config
    // ====================

    Config::clearColor = vec3(0.53f, 0.81f, 0.92f);



    // ====================
    //  Création fenêtre
    // ====================

    GameWindow game;

    Scene* scene = game.getScene();
    
    Camera* camera = game.getCamera();
    camera->setPerspective(60, (float)Config::screenWidth/Config::screenHeight, 0.001, 1000);


    // ======================
    //  Définition skymap
    // ======================

    scene->setSkyBox(Config::PATH_SKYBOX+"/sky.jpg");
    // scene->setSkyBox(Config::PATH_SKYBOX+"/space.jpg");




    // ======================
    //  Chargement textures
    // ======================

    Texture tex_minecraft("Resources/Textures/minecraft.png");




    // ====================
    //  Construction scene
    // ====================

    GameObject* go;
    GameObject* player;

    // Player
    player = new GameObject(scene);
    player->transform.translate(vec3(0.0f,20.0f,0.0f));
    new FirstPersonCamera(player,camera);

    // Map
    go = new GameObject(scene);
    new MineMap(go,player,16,16,256,time(NULL));

    // Light
    // go = new GameObject(scene);
    // go->transform.translate(vec3(0,300,0));
    // PointLight* light = new PointLight(go);
    // light->setAttenuation(0.001);


    game.run();

    return 0;
}
