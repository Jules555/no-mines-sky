#pragma once

#include <map>
#include <thread>

#include <Behavior/behavior.h>
#include <Graphic/graphic.h>
#include <Block/blocks.h> 
#include <Noise/noise.h> // Bibliothèque pour la génération de bruit qui servira pour la génération procédurale de carte
#include "chunk.h" 

using namespace std;
using namespace engine;
using namespace noise;

class Chunk;


class MineMap : public Behavior
{
    friend class Chunk;
  
    public:

        MineMap(GameObject* go, GameObject* player, uint cx, uint cy, uint cd, int s=time(NULL));
        
        virtual void subscribe();
        virtual void start();
        virtual void update();
        virtual void render() const;
      

    protected:

        // Target player
        GameObject* player;

        // Texture
        Texture* tex_minecraft;

        // Blocks collection
        Block* blocks[Blocks::BlockType::COUNT]; // Stocke tous les types de blocks

        // Chunks 
        int chunkSizeX;
        int chunkSizeY; 
        int chunkDepth;
        map< int,map< int,Chunk* > > chunks; // Associe une position dans l'espace à un chunk
        bool chunkExist(int x, int z); // Vérifie si un block est présent à la position donnée

        // Générateur de bruit de Perlin
        int seed;
        module::Perlin terrainGenerator; 
        module::Perlin blockGenerator; 

        // Stats
        unsigned int nbBlocks;

    private : 

        // Le thread de génération du terrain !
        void loadMapAsync();
        thread threadMapGeneration;

};
