#pragma once

#include <map>

#include "Utility/glm.h"
#include "Utility/random.h"
#include "Block/blocks.h"
#include "minemap.h"

using namespace std;
using namespace glm;

class MineMap;

class Chunk
{
	private : 

		// Map 
		MineMap* minemap;

		// Position
		ivec2 position;

		// Chunk state
	    bool modified;
	    bool bufferized;

	    // Blocks
        Blocks::BlockType*** voxels; // Associe une position dans l'espace à un type de block
        bool voxelExist(int x, int y, int z); // Vérifie si un block est présent à la position donnée

        // Vertex infos
        vector<vec3> vertices;
        vector<vec2> texels;
        vector<float> adjacents;

        // Buffers
        GLuint vbo;
        GLuint vao;

        // Stats
        unsigned int nbBlocks;


    public : 

    	Chunk(MineMap* _map, int _x, int _z);

    	void generate();
    	void build();
    	void update();
    	void buffer();
    	void render() const;

        inline unsigned int countBlocks() {return nbBlocks;}
};
