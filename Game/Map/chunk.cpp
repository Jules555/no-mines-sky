#include "chunk.h"
#include "minemap.h"


Chunk::Chunk(MineMap* _map, int _x, int _z)
:minemap(_map),modified(false),bufferized(false),vbo(0),vao(0),nbBlocks(0)
{
	voxels = new Blocks::BlockType**[minemap->chunkSizeX+2];
	for(int i=0; i<minemap->chunkSizeX+2; ++i)
	{
		voxels[i] = new Blocks::BlockType*[minemap->chunkDepth];
		for(int j=0; j<minemap->chunkDepth; ++j)
		{
			voxels[i][j] = new Blocks::BlockType[minemap->chunkSizeY+2];
			for(int k=0; k<minemap->chunkSizeY+2; ++k)
				voxels[i][j][k] = Blocks::BlockType::NONE;
		}
	}

	position = ivec2(_x*minemap->chunkSizeX, _z*minemap->chunkSizeY);

	/* On veux générer tous les blocs autour de notre chunk pour le calcul des faces :

			[X][X][X][X][X]
			[X][C][C][C][X]
			[X][C][C][C][X]
			[X][C][C][C][X]
			[X][X][X][X][X]

			 chunk vu du dessus, [C] --> Block du chunk , [X] --> Généré en plus pour le calcule des faces
			 On doit donc se positionner en haut à gauche :
	*/
	position.x -= 1;
	position.y -= 1;

	generate();
}



void Chunk::generate()
{
	float factorXZ = 0.01f;
	float factorY  = 0.01f;

	// On va maintenant générer des blocks de 0 à chunckSixeX/Y +2  pour avoir le bon nombre de blocks
	for(int i=1; i<=minemap->chunkSizeX+2; ++i)
		for(int j=0; j<minemap->chunkDepth; ++j)
			for(int k=1; k<=minemap->chunkSizeY+2; ++k)
			{
				// Au niveau 0 on a forcement de la bedrock
				if(j == 0)
				{
					voxels[i-1][j][k-1] = Blocks::BEDROCK;
					continue;
				}

				float nx = (position.x + i) * factorXZ;
				float nz = (position.y + k) * factorXZ;
				float ny = j * factorY;

				float offset = (minemap->chunkDepth/2 - j) * factorY;
				
				float density = minemap->terrainGenerator.GetValue(nx,ny,nz) + offset;

				if(density > 0)
				{
					float block = minemap->blockGenerator.GetValue(nx,ny,nz);

					if(block > 0.45)
						voxels[i-1][j][k-1] = Blocks::STONE;
					else
						voxels[i-1][j][k-1] = Blocks::GRASS;

					nbBlocks++;
				}
			}

	minemap->nbBlocks += nbBlocks;

	// if(voxels.size() <= 0)
	// 	cout << "Aucun voxel pour le chunk " << position.x << " " << position.y << endl;

	build();
}



bool Chunk::voxelExist(int x, int y, int z)
{
	return y >= 0 && y < minemap->chunkDepth && voxels[x][y][z] != Blocks::BlockType::NONE;
}



void Chunk::build()
{
	modified = false;

	vertices.clear();
	texels.clear();
	adjacents.clear();

	int tmp_adj[4];

	// On itère sur tous les voxels (sauf ceux qui servent uniquement pour les tests d'existence)
	for(int x=1; x<=minemap->chunkSizeX; ++x)
	for(int y=0; y<minemap->chunkDepth;  ++y)
	for(int z=1; z<=minemap->chunkSizeY; ++z)
	{
		// S ipas de bloc à cette position on ne fait rien
		if(!voxelExist(x,y,z))
			continue;

		// Plus simple à manipuler
		Blocks::BlockType type = voxels[x][y][z];
		Block* bloc = minemap->blocks[type];
		vec3 offset = (vec3(x-1,y,z-1) + vec3(position.x,0,position.y)) * 0.1f;

		// On ne construit que les faces visibles

		// TOP
		if(!voxelExist(x,y+1,z))
		{
			uint index = bloc->getIndex(Block::TOP);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x-1,y+1,z)) tmp_adj[0]++;
			if(voxelExist(x,y+1,z-1)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x-1,y+1,z-1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x+1,y+1,z)) tmp_adj[1]++;
			if(voxelExist(x,y+1,z-1)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x+1,y+1,z-1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x-1,y+1,z)) tmp_adj[2]++;
			if(voxelExist(x,y+1,z+1)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x-1,y+1,z+1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x+1,y+1,z)) tmp_adj[3]++;
			if(voxelExist(x,y+1,z+1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x+1,y+1,z+1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}

		// (Aucun rapport) Si on est sur un voxel d'herbe avec un autre bloc au dessus, alors on le transforme en terre
		else if(type == Blocks::GRASS)
		{
			type = Blocks::DIRT;
			bloc = minemap->blocks[type];
		}

		// BOTTOM
		if(!voxelExist(x,y-1,z))
		{
			uint index = bloc->getIndex(Block::BOTTOM);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x-1,y-1,z)) tmp_adj[0]++;
			if(voxelExist(x,y-1,z+1)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x-1,y-1,z+1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x+1,y-1,z)) tmp_adj[1]++;
			if(voxelExist(x,y-1,z+1)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x+1,y-1,z+1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x-1,y-1,z)) tmp_adj[2]++;
			if(voxelExist(x,y-1,z-1)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x-1,y-1,z-1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x+1,y-1,z)) tmp_adj[3]++;
			if(voxelExist(x,y-1,z-1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x+1,y-1,z-1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}

		// LEFT
		if(!voxelExist(x-1,y,z))
		{
			uint index = bloc->getIndex(Block::LEFT);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x-1,y,z-1)) tmp_adj[0]++;
			if(voxelExist(x-1,y+1,z)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x-1,y+1,z-1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x-1,y,z+1)) tmp_adj[1]++;
			if(voxelExist(x-1,y+1,z)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x-1,y+1,z+1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x-1,y,z-1)) tmp_adj[2]++;
			if(voxelExist(x-1,y-1,z)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x-1,y-1,z-1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x-1,y-1,z)) tmp_adj[3]++;
			if(voxelExist(x-1,y,z+1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x-1,y-1,z+1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}

		// RIGHT
		if(!voxelExist(x+1,y,z))
		{
			uint index = bloc->getIndex(Block::RIGHT);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x+1,y+1,z)) tmp_adj[0]++;
			if(voxelExist(x+1,y,z+1)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x+1,y+1,z+1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x+1,y+1,z)) tmp_adj[1]++;
			if(voxelExist(x+1,y,z-1)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x+1,y+1,z-1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x+1,y-1,z)) tmp_adj[2]++;
			if(voxelExist(x+1,y,z+1)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x+1,y-1,z+1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x+1,y-1,z)) tmp_adj[3]++;
			if(voxelExist(x+1,y,z-1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x+1,y-1,z-1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}

		// FRONT
		if(!voxelExist(x,y,z+1))
		{
			uint index = bloc->getIndex(Block::FRONT);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x-1,y,z+1)) tmp_adj[0]++;
			if(voxelExist(x,y+1,z+1)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x-1,y+1,z+1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x+1,y,z+1)) tmp_adj[1]++;
			if(voxelExist(x,y+1,z+1)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x+1,y+1,z+1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x-1,y,z+1)) tmp_adj[2]++;
			if(voxelExist(x,y-1,z+1)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x-1,y-1,z+1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x+1,y,z+1)) tmp_adj[3]++;
			if(voxelExist(x,y-1,z+1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x+1,y-1,z+1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}

		// BACK
		if(!voxelExist(x,y,z-1))
		{
			uint index = bloc->getIndex(Block::BACK);

			// Calcul adjacence pour les 4 points de la face
			tmp_adj[0] = tmp_adj[1] = tmp_adj[2] = tmp_adj[3] = 0;

			// Premier point
			if(voxelExist(x+1,y+1,z-1)) tmp_adj[0]++;
			if(voxelExist(x,y,z-1)) tmp_adj[0]++;
			if(tmp_adj[0] == 2) tmp_adj[0]++;
			else if(voxelExist(x+1,y+1,z-1)) tmp_adj[0]++;

			// Deuxième point
			if(voxelExist(x-1,y,z-1)) tmp_adj[1]++;
			if(voxelExist(x,y+1,z-1)) tmp_adj[1]++;
			if(tmp_adj[1] == 2) tmp_adj[1]++;
			else if(voxelExist(x-1,y+1,z-1)) tmp_adj[1]++;

			// Troisième point
			if(voxelExist(x+1,y,z-1)) tmp_adj[2]++;
			if(voxelExist(x,y-1,z-1)) tmp_adj[2]++;
			if(tmp_adj[2] == 2) tmp_adj[2]++;
			else if(voxelExist(x+1,y-1,z-1)) tmp_adj[2]++;

			// // Quatrième point
			if(voxelExist(x-1,y,z-1)) tmp_adj[3]++;
			if(voxelExist(x,y-1,z-1)) tmp_adj[3]++;
			if(tmp_adj[3] == 2) tmp_adj[3]++;
			else if(voxelExist(x-1,y-1,z-1)) tmp_adj[3]++;

			// On ajoute tout dans nos vectors
			for(uint i=0; i<6; ++i)
			{
				vertices.push_back(bloc->vertices[index+i]+offset);
				texels.push_back(bloc->texels[index+i]);

				switch(i)
				{
					case 0 :
					case 3 : adjacents.push_back(tmp_adj[0]); break;
					case 5 : adjacents.push_back(tmp_adj[1]); break;
					case 1 : adjacents.push_back(tmp_adj[2]); break;
					case 2 : 
					case 4 : adjacents.push_back(tmp_adj[3]); break;
				}
			}
		}
	}
}


void Chunk::update()
{
	if(!bufferized)
		buffer();
}


void Chunk::buffer()
{
	if(vertices.size() <= 0)
		return;

	// === VBO ================

    if(glIsBuffer(vbo) == GL_TRUE)
        glDeleteBuffers(1, &vbo);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    int verticesBytesSize = vertices.size() * sizeof(vec3);
    int texelsBytesSize = texels.size() * sizeof(vec2);
    int adjacentsBytesSize = adjacents.size() * sizeof(float);

    glBufferData(GL_ARRAY_BUFFER, verticesBytesSize + texelsBytesSize + adjacentsBytesSize, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, verticesBytesSize, vertices.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytesSize, texelsBytesSize, texels.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytesSize + texelsBytesSize, adjacentsBytesSize, adjacents.data());

    glBindBuffer(GL_ARRAY_BUFFER, 0);


    // === VAO ================

    // Destruction d'un éventuel ancien VAO
    if(glIsVertexArray(vao) == GL_TRUE)
        glDeleteVertexArrays(1, &vao);

    // Chargement VAO
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER,vbo);

    Shader* shader = ShaderManager::getShader(ShaderManager::MINECRAFT);

    // Accès aux vertices dans la mémoire vidéo
    GLint posAttrib = glGetAttribLocation(shader->getProgram(), "vertex");
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(posAttrib);

	// Accès aux texels dans la mémoire vidéo
    GLint texAttrib = glGetAttribLocation(shader->getProgram(), "texel");
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(verticesBytesSize));
    glEnableVertexAttribArray(texAttrib);

    // Accès au nombre de cubes adjacents au vertex dans la mémoire vidéo
    GLint adjAttrib = glGetAttribLocation(shader->getProgram(), "adjacent");
    glVertexAttribPointer(adjAttrib, 1, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(verticesBytesSize + texelsBytesSize));
    glEnableVertexAttribArray(adjAttrib);

    // Déverrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage du VAO
    glBindVertexArray(0);

   // Update chunk state
    bufferized = true;
}



void Chunk::render() const
{
	if(!bufferized)
		return;

	glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    glBindVertexArray(0);
}
