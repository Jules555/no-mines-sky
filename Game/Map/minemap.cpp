#include "minemap.h"
#include "chunk.h"


MineMap::MineMap(GameObject* go, GameObject* _player, uint _chunkX, uint _chunkY, uint _chunkDepth, int s)
:Behavior(go),player(_player),chunkSizeX(_chunkX),chunkSizeY(_chunkY),chunkDepth(_chunkDepth),seed(s),nbBlocks(0)
{
	tex_minecraft = new Texture("Resources/Textures/minecraft.png",true);
	subscribe();
}


void MineMap::subscribe()
{
	gameObject->getScene()->subscribeRender(this);
}


void MineMap::start()
{
	// Setup Perlin modules

	// Module de génération du terrain
	terrainGenerator.SetSeed(seed);
	terrainGenerator.SetFrequency(0.35);

	// Module de génération de type de block
	blockGenerator.SetSeed(seed);
	blockGenerator.SetFrequency (4);
	blockGenerator.SetPersistence (0.1);

	// Tableau contenant tous les types de voxels
	for(int i=0; i<(int)Blocks::BlockType::COUNT; ++i)
		blocks[i] = Blocks::buildBlock((Blocks::BlockType)i);

	// On lance le thread de génération du terrain
	threadMapGeneration = thread(&MineMap::loadMapAsync,this);
	threadMapGeneration.detach();
}



void MineMap::loadMapAsync()
{
	chunks[0][0]  = new Chunk(this,0,0);

	int d=1;
	while(d < 10)
	{
		for(int i=-d; i<=d; ++i)
		{
			chunks[i][d]  = new Chunk(this,i,d);
			chunks[i][-d] = new Chunk(this,i,-d);
		}

		for(int i=-d+1; i<d; ++i)
		{
			chunks[d][i]  = new Chunk(this,d,i);
			chunks[-d][i] = new Chunk(this,-d,i);
		}

		d++;
	}

	cout << nbBlocks << " blocks" << endl;

	// for(int i=playerPos.x-5; i<=playerPos.x+5; ++i)
	// 	for(int j=playerPos.y-5; j<=playerPos.y+5; ++j)
	// 		chunks[i][j] = new Chunk(this,i,j);
}




bool MineMap::chunkExist(int x, int z)
{
	if(chunks.count(x))
		if(chunks[x].count(z))
			return true;
	return false;
}



void MineMap::update()
{
	map< int,map< int,Chunk* > >::iterator itx;
	map< int,Chunk* >::iterator itz;
	for(itx = chunks.begin(); itx != chunks.end(); ++itx)
		for(itz = itx->second.begin(); itz != itx->second.end(); ++itz)
			itz->second->update();
}




void MineMap::render() const
{
	if(ShaderManager::getCurrentShader() != ShaderManager::getShader(ShaderManager::MINECRAFT))
		return;

    // Texture
    glBindTexture(GL_TEXTURE_2D, tex_minecraft->getID());

    // Envoi de la matrice modelview
    Camera* camera = gameObject->scene->camera;
    mat4 modelview = camera->getLook() * gameObject->transform.getMatrix();
    ShaderManager::getCurrentShader()->setUniform("modelview",modelview);

    // Rendu de chaque chunk
    map< int,map< int,Chunk* > >::const_iterator itx;
	map< int,Chunk* >::const_iterator itz;
	for(itx = chunks.begin(); itx != chunks.end(); ++itx)
		for(itz = itx->second.begin(); itz != itx->second.end(); ++itz)
			itz->second->render();
}
